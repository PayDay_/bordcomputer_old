﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Bordcomputer
{
    public class Button : Component
	{
		public static List<Button> buttons = new List<Button>();

		public Image Background;
		public Image Foreground;

        public override Vector2 Position
        {
            get { return base.Position; }
            set { base.Position = value; }
        }
        public override Vector2 Size
        {
            get { return base.Size; }
            set { base.Size = value; }
        }

		public delegate void ClickDelegate(object sender);
		public event ClickDelegate OnButtonClick;

        //Pressing effect
        bool pressed;
        bool pressEffect;
        float pressEffectProgress;
        Color colorDown = Color.Gray;
        Color colorDownForeground = Color.Gray;

        public Button (Vector2 position, Vector2 size, string backgroundPath, string foregroundPath, Component parent = null, int depth = 0) : base(position, size, parent)
		{
            buttons.Insert(0, this);

            Background = new Image (Vector2.Zero, size, backgroundPath, this, false, depth);
            Foreground = new Image (Vector2.Zero, size, foregroundPath, this, false, depth);
			Parent = parent;
		}

		public Button (Vector2 position, Vector2 size, string backgroundPath) : base(position, size)
        {
            buttons.Insert(0, this);

			Background = new Image (Vector2.Zero, size, backgroundPath, this);
		}

		public Button(Vector2 position, Vector2 size, Color backgroundColor, Component parent = null) : base(position, size, parent)
        {
            buttons.Insert(0, this);

            Background = new Image(Vector2.Zero, size, backgroundColor, this);
			Parent = parent;

            colorDown = Color.FromArgb((int)Math.Round((float)backgroundColor.R / 2), (int)Math.Round((float)backgroundColor.G / 2), (int)Math.Round((float)backgroundColor.B / 2));
        }

        public Button(Vector2 position, Vector2 size, Color backgroundColor, string foregroundPath, Component parent = null) : base(position, size, parent)
        {
            buttons.Insert(0, this);

            Background = new Image(Vector2.Zero, size, backgroundColor, this);
            Foreground = new Image(Vector2.Zero, size, foregroundPath, this);
            Parent = parent;

            colorDown = Color.FromArgb((int)Math.Round((float)backgroundColor.R / 2), (int)Math.Round((float)backgroundColor.G / 2), (int)Math.Round((float)backgroundColor.B / 2));
        }

        public static void OnDown(Vector2 position)
		{
			foreach (Button button in buttons) 
			{
				if (button.Visible) 
				{
					if (position.X - button.GlobalPosition.X >= 0 && position.X - (button.GlobalPosition.X + button.Size.X) <= 0
						&& position.Y - button.GlobalPosition.Y >= 0 && position.Y - (button.GlobalPosition.Y + button.Size.Y) <= 0)
                    {
                        button.pressed = true;

                        if (button.Background != null)
                            button.Background.Drawable.Color = button.colorDown;
                        if (button.Foreground != null)
                            button.Foreground.Drawable.Color = button.colorDownForeground;
                        break;
					}
				}
			}
		}
			
		public static void OnUp(Vector2 position)
		{
            Button[] buttonsSaved = buttons.ToArray();
            foreach (Button button in buttonsSaved) 
			{
                if (button.pressed)
                {
                    button.pressEffect = true;

                    if (position.X - button.GlobalPosition.X >= 0 && position.X - (button.GlobalPosition.X + button.Size.X) <= 0 && position.Y - button.GlobalPosition.Y >= 0 && position.Y - (button.GlobalPosition.Y + button.Size.Y) <= 0)
                        button.OnButtonClick?.Invoke(button);
                }

                button.pressed = false;
			}
		}

        public override void Update()
        {
            base.Update();

            UpdatePressEffect();
        }

        private void UpdatePressEffect()
        {
            if (!pressEffect)
                return;

            int red = (int)Math.Round(MathHelper.SmoothStep(colorDown.R, Background.Drawable.OriginalColor.R, pressEffectProgress));
            int green = (int)Math.Round(MathHelper.SmoothStep(colorDown.G, Background.Drawable.OriginalColor.G, pressEffectProgress));
            int blue = (int)Math.Round(MathHelper.SmoothStep(colorDown.B, Background.Drawable.OriginalColor.B, pressEffectProgress));

            int redForeground = 0, greenForeground = 0, blueForeground = 0;

            if (Foreground != null)
            {
                redForeground = (int)Math.Round(MathHelper.SmoothStep(colorDownForeground.R, Foreground.Drawable.OriginalColor.R, pressEffectProgress));
                greenForeground = (int)Math.Round(MathHelper.SmoothStep(colorDownForeground.G, Foreground.Drawable.OriginalColor.G, pressEffectProgress));
                blueForeground = (int)Math.Round(MathHelper.SmoothStep(colorDownForeground.B, Foreground.Drawable.OriginalColor.B, pressEffectProgress));
            }

            pressEffectProgress += (float)Program.ElapsedSeconds * 6;
            if (pressEffectProgress >= 1)
            {
                pressEffect = false;
                pressEffectProgress = 0;
                red = Background.Drawable.OriginalColor.R;
                green = Background.Drawable.OriginalColor.G;
                blue = Background.Drawable.OriginalColor.B;

                if (Foreground != null)
                {
                    redForeground = Foreground.Drawable.OriginalColor.R;
                    greenForeground = Foreground.Drawable.OriginalColor.G;
                    blueForeground = Foreground.Drawable.OriginalColor.B;
                }
            }


            if (Background != null)
            {
                Color newColor = Color.FromArgb(red, green, blue);
                Background.Drawable.Color = newColor;
            }
            if (Foreground != null)
            {
                Color newColorForeground = Color.FromArgb(redForeground, greenForeground, blueForeground);
                Foreground.Drawable.Color = newColorForeground;
            }
        }
    }
}

