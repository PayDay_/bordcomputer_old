﻿using System;
using OpenTK;
using System.Drawing;
using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Text;

namespace Bordcomputer
{
	public class Label : Component
	{
		public static List<Label> Labels = new List<Label>();

		private string text;
		public Font Font;
        public int FontSize;
        public StringAlignment Alignment;
        public Color Color;

		public string Text
		{
			set
			{
				this.text = value;
			}
			get
			{
				return text;
			}
		}

		public Label (string text, Vector2 position, Color color, Font font, int fontSize, Component parent = null, int depth = 0, StringAlignment alignment = StringAlignment.Near) : base(position, Vector2.One, parent)
		{
			this.text = text;
			this.Font = font;
            this.FontSize = fontSize;
            this.Alignment = alignment;
            this.Color = color;

            Labels.Add(this);
        }
		/*private void RenderText(bool overrideVisibility = false)
		{
			if (!Visible && overrideVisibility == false)
				return;

            if (Drawable.Texture != -1)
                GL.DeleteTexture(Drawable.Texture);

			// Create Bitmap and OpenGL texture
			//Bitmap bitmap = new Bitmap((int)Size.X, (int)Size.Y); // match window size
			Bitmap bitmap = new Bitmap((int)Size.X, (int)Size.Y);

			Drawable.Texture = GL.GenTexture();
			GL.BindTexture(TextureTarget.Texture2D, Drawable.Texture);
			using (Graphics gfx = Graphics.FromImage(bitmap))
			{
				gfx.Clear(Color.Transparent);

                gfx.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
                gfx.SmoothingMode = SmoothingMode.AntiAlias;

                StringFormat format = StringFormat.GenericDefault;
                format.Alignment = Alignment;

                float x = 0;
                if (Alignment == StringAlignment.Center)
                    x = Size.X / 2;

				gfx.DrawString(text, Font, Brushes.White, x, 0, format);
			}

			BitmapData data = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, (int)Size.X, (int)Size.Y, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0); 
			bitmap.UnlockBits(data);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);

            //GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            //GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
        }*/
    }
}

