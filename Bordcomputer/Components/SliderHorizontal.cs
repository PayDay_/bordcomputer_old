﻿using OpenTK;
using System.Collections.Generic;
using System.Drawing;
using System;

namespace Bordcomputer
{
    public class SliderHorizontal : Component
    {
        public static List<SliderHorizontal> SlidersHorizontal = new List<SliderHorizontal>();

        Image left;
        Image right;
        Image middle;

        Image bar;
        Image handle;

        public float BarSize;

        public bool dragged;
        public static int DraggedCount;

        float value;
        public float Value
        {
            set
            {
                this.value = value;
                UpdateBar();
            }
            get
            {
                return value;
            }
        }

        public float Percent
        {
            get
            {
                return value / BarSize;
            }
            set
            {
                Value = value * BarSize;
            }
        }

        public delegate void ClickDelegate(object sender);
        public event ClickDelegate OnDownEvent;
        public event ClickDelegate OnMoveEvent;
        public event ClickDelegate OnUpEvent;

        public SliderHorizontal(Vector2 position, float size, Color barColor, Component parent = null, int depth = 0) : base(position, new Vector2(size, 40), parent)
        {
            SlidersHorizontal.Add(this);

            Position = position;
            Size = new Vector2(size, 40);

            left = new Image(Vector2.Zero, new Vector2(20, 40), "sliderLeft.png", this, false, depth);
            middle = new Image(Vector2.Zero, new Vector2(size - 20 * 2, 40), "slider.png", this, false, depth + 1);
            right = new Image(Vector2.Zero, new Vector2(20, 40), "sliderRight.png", this, false, depth);

            bar = new Image(new Vector2(20, 0), new Vector2(size - 20 * 2, 40), barColor, this, depth);
            handle = new Image(Vector2.Zero, new Vector2(40), "handle.png", this, false, depth);

            BarSize = size - 20 * 2;
            UpdateSliderWidth();

            Value = 0;
        }

        private void UpdateSliderWidth()
        {
            left.Position = Vector2.Zero;
            middle.Position = new Vector2(20, 0);
            right.Position = new Vector2(20 + BarSize, 0);
        }

        void UpdateBar()
        {
            if (value < 0)
                value = 0;

            if (value > BarSize)
                value = BarSize;

            bar.Size = new Vector2(value, 40);
            handle.Position = new Vector2((int)Math.Round(value), handle.Position.Y);
        }

        public static void OnDown(Vector2 position)
        {
            foreach (SliderHorizontal slider in SlidersHorizontal)
            {
                if (!slider.Enabled)
                    return;

                if (slider.Visible)
                {
                    if (MathHelper.IsPointInArea(position, slider.bar.GlobalPosition, new Vector2(slider.BarSize, 40)) || MathHelper.IsPointInArea(position, slider.handle.GlobalPosition, slider.handle.Size))
                    {
                        DraggedCount++;
                        slider.dragged = true;
                        slider.Value = position.X - slider.bar.GlobalPosition.X;

                        if(slider.OnDownEvent != null)
                            slider.OnDownEvent(slider);
                        break;
                    }
                }
            }
        }

        public static void OnMove(Vector2 position)
        {
            foreach (SliderHorizontal slider in SlidersHorizontal)
            {
                if (!slider.Enabled)
                    return;

                if (slider.Visible)
                {
                    if (slider.dragged)
                    {
                        slider.Value = position.X - slider.bar.GlobalPosition.X;

                        if(slider.OnMoveEvent != null)
                            slider.OnMoveEvent(slider);
                        break;
                    }
                }
            }
        }

        public static void OnUp(Vector2 position)
        {
            foreach (SliderHorizontal slider in SlidersHorizontal)
            {
                if (!slider.Enabled)
                    return;

                if (slider.Visible)
                {
                    if (slider.dragged)
                    {
                        DraggedCount--;
                        slider.dragged = false;

                        if(slider.OnUpEvent != null)
                            slider.OnUpEvent(slider);
                        break;
                    }
                }
            }
        }
    }
}

