﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Bordcomputer
{
    public class ScrollList : Component
	{
		public static List<ScrollList> ScrollLists = new List<ScrollList>();

		new public List<Component> Components = new List<Component>();

		public Image Panel;
		public SliderVertical Slider;

		public bool Down;
		public bool Dragged;

        private float scrollHeight;
        public float ScrollHeight
        {
            get 
            {
                return scrollHeight;
            }
            set
            {
                scrollHeight = value;
                if (value <= 0)
                {
                    Enabled = false;
                    Slider.Enabled = false;
                    Value = 0;
                }
                else
                {
                    Enabled = true;
                    Slider.Enabled = true;
                    Value = 0;
                }
            }
        }

		float lastTouchHeight;

		//Sliding after release
		private bool sliding;
		private float slidingProgress;
		private float slideFrom;
		private float slideTo;
        private float scrollSpeed;

        private float scrollSpeedLastValue;
        private float scrollSpeedTimer;

		float value;
		public float Value
		{
			set 
			{
				this.value = value;
                if(Slider != null)
                    Slider.Value = value;
				UpdateList ();
			}
			get 
			{
				return value;
			}
		}

		public delegate void ClickDelegate(object sender);
		public event ClickDelegate OnMoveEvent;

		public ScrollList (Vector2 position, Vector2 size, float scrollHeight, Color backgroundColor, Component parent = null) : base(position, size, parent)
        {
			this.Position = position;
			this.Size = size;

			Slider = new SliderVertical (Vector2.Zero, size.Y, Color.White, this, 1);
            Panel = new Image (Vector2.Zero, size, backgroundColor, new RectangleF(GlobalPosition.X, GlobalPosition.Y, Size.X, Size.Y), this);	
            Slider.OnDownEvent += (object sender) => OnSliderDown(sender);
            Slider.OnMoveEvent += (object sender) => OnSliderMove(sender);

            this.ScrollHeight = scrollHeight;

			ScrollLists.Add (this);
		}
			
		void UpdateList ()
		{
            if (Slider == null)
                return;

			float heightOffset = Slider.Value / Slider.BarSize * ScrollHeight;
            Panel.Position = new Vector2 (Panel.Position.X, (float)Math.Round(Panel.StartPosition.Y - heightOffset));
		}

		public override void Update()
		{
			base.Update();

            UpdateScrollSpeed();
			UpdateSliding ();
        }

		public static void OnDown(Vector2 position)
		{
			foreach (ScrollList scrollList in ScrollLists)
			{
                if (!scrollList.Slider.Visible)
                    continue;
                if (!scrollList.Enabled)
                    continue;

				if (scrollList.Visible) 
				{
                    if(MathHelper.IsPointInArea(position, scrollList.GlobalPosition, scrollList.Size) && !MathHelper.IsPointInArea(position, scrollList.Slider.GlobalPosition, scrollList.Slider.Size))
					{
						scrollList.Down = true;
						scrollList.lastTouchHeight = position.Y;
						scrollList.sliding = false;
						break;
					}
				}
			}
		}

		public static void OnMove(Vector2 position)
		{
			foreach (ScrollList scrollList in ScrollLists) 
			{
                if (!scrollList.Slider.Visible)
                    continue;
                if (!scrollList.Enabled)
                    continue;

                if (scrollList.Visible) 
				{
					if (scrollList.Down) 
					{
						if (scrollList.Dragged) 
						{
                            scrollList.Slider.Percent += (scrollList.lastTouchHeight - position.Y) / scrollList.ScrollHeight;
							scrollList.lastTouchHeight = position.Y;

                            if(scrollList.OnMoveEvent != null)
							    scrollList.OnMoveEvent (scrollList);
						} 
						else 
						{
							if (scrollList.lastTouchHeight - position.Y > 1 || scrollList.lastTouchHeight - position.Y < -1) 
							{
								scrollList.Dragged = true;
							}
						}

                        scrollList.Value = scrollList.Slider.Value;
                        scrollList.UpdateVisibilityOptimizations();

                        break;
					}
				}
			}
		}

		public static void OnUp(Vector2 position)
		{
			foreach (ScrollList scrollList in ScrollLists) 
			{
                if (!scrollList.Slider.Visible)
                    continue;

                if (scrollList.Visible) 
				{
                    if (scrollList.Down)
                    {
                        scrollList.Down = false;
                        scrollList.Dragged = false;
                        scrollList.sliding = true;
                        scrollList.slidingProgress = 0.5F;
                        scrollList.slideFrom = scrollList.Value + scrollList.scrollSpeed * 24;
                        scrollList.slideTo = scrollList.Value - scrollList.scrollSpeed * 24;
                        break;
                    }
				}
			}
		}

        private void OnSliderDown(object sender)
        {
            Value = Slider.Value;
            UpdateVisibilityOptimizations();
        }

        private void OnSliderMove(object sender)
        {
            Value = Slider.Value;
            UpdateVisibilityOptimizations();
        }

        private void UpdateScrollSpeed()
        {
            if (!Dragged)
                return;

            scrollSpeedTimer += (float)Program.ElapsedSeconds;
            if (scrollSpeedTimer < 0.01f)
                return;

            scrollSpeed = scrollSpeedLastValue - Slider.Value;
            scrollSpeedLastValue = Slider.Value;
            scrollSpeedTimer = 0;
        }

		private void UpdateSliding()
		{
			if (!sliding)
				return;

			float scrollValue = 0;

			scrollValue = MathHelper.SmoothStep(slideFrom, slideTo, slidingProgress);

			Value = scrollValue;

            slidingProgress += (float)Program.ElapsedSeconds / 2;
			if (slidingProgress >= 1)
			{
                Value = slideTo;

				slideTo = 0;
				slideFrom = 0;
				sliding = false;
				slidingProgress = 0;
			}

            UpdateVisibilityOptimizations();
        }

        private void UpdateVisibilityOptimizations()
        {
            float currentPosY = Slider.Percent * ScrollHeight + Size.Y / 2;
            foreach(Component component in Components)
            {
                component.Visible = true;
                float distance = Math.Abs((component.GlobalPosition.Y + currentPosY) - currentPosY);
                if (distance >= Size.Y * 2)
                    component.Visible = false;
            }
        }

        protected override void VisibleChanged(bool visible)
        {
            base.VisibleChanged(visible);

            if(visible)
                UpdateVisibilityOptimizations();
        }
    }
}

