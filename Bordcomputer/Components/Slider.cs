﻿using System;
using OpenTK;
using System.Collections.Generic;
using System.Drawing;

namespace Bordcomputer
{
	public class Slider
	{
		public static List<Slider> sliders = new List<Slider>();

		Image background;
		Image bar;
		Image handle;

		Vector2 barSize;

		public bool dragged;

		public bool Visible
		{
			set {
				background.visible = value;
				bar.visible = value;
				handle.visible = value;
			}
			get {
				return (background.visible || bar.visible || handle.visible);
			}
		}

		float value;
		public float Value
		{
			set 
			{
				this.value = value;
				UpdateBar ();
			}
			get 
			{
				return value;
			}
		}

		public float Percent
		{
			get 
			{
				return value / barSize.X * 100;
			}
			set
			{
				Value = value * barSize.X / 100;
			}
		}

		public delegate void ClickDelegate(object sender);
		public event ClickDelegate OnDownEvent;
		public event ClickDelegate OnUpEvent;

		public Slider (Vector2 position, Vector2 backgroundSize, string backgroundPath, Vector2 handleSize, string handlePath, Vector2 barSize, string barPath, Vector2 barOffset)
		{
			sliders.Add (this);
			background = new Image (position, backgroundSize, backgroundPath);
			bar = new Image (position + barOffset, barSize, barPath);
			handle = new Image (position, handleSize, handlePath);

			this.barSize = barSize;

			Value = 0;
		}

		void UpdateBar()
		{
			if (value < 0)
				value = 0;

			if (value > barSize.X)
				value = barSize.X;

			bar.Size = new Vector2(value, barSize.Y);

			float handlePosX = background.Position.X + value;
			handle.Position = new Vector2 (handlePosX, handle.Position.Y);
		}

		public void OnUpdateFrame(FrameEventArgs e)
		{
		}

		public static void OnDown(Vector2 position)
		{
			foreach (Slider slider in sliders) 
			{
				if (slider.Visible) 
				{
					if(Math.IsPointInArea(position, slider.bar.Position, slider.barSize) || Math.IsPointInArea(position, slider.handle.Position, slider.handle.Size))
					{
						//if (slider.OnDownEvent != null) 
						//{
							//slider.OnDownEvent (slider);
							slider.dragged = true;
							slider.Value = position.X - slider.bar.Position.X;
							break;
						//}
					}
				}
			}
		}

		public static void OnMove(Vector2 position)
		{
			foreach (Slider slider in sliders) 
			{
				if (slider.Visible) 
				{
					if (slider.dragged) 
					{
						slider.Value = position.X - slider.bar.Position.X;
						break;
					}
				}
			}
		}

		public static void OnUp(Vector2 position)
		{
			foreach (Slider slider in sliders) 
			{
				if (slider.Visible) 
				{
					if (slider.dragged) 
					{
						slider.OnUpEvent (slider);
						slider.dragged = false;
						break;
					}
				}
			}
		}
	}
}

