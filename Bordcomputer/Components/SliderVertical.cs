﻿using OpenTK;
using System.Collections.Generic;
using System.Drawing;

namespace Bordcomputer
{
    public class SliderVertical : Component
	{
		public static List<SliderVertical> SlidersVertical = new List<SliderVertical>();

		Image top;
        Image bottom;
        Image middle;

		Image bar;
		Image handle;

		public float BarSize;

		public bool dragged;
        public static int DraggedCount;

		float value;
		public float Value
		{
			set 
			{
				this.value = value;
				UpdateBar ();
			}
			get 
			{
				return value;
			}
		}

		public float Percent
		{
			get 
			{
				return value / BarSize;
			}
			set
			{
				Value = value * BarSize;
			}
		}

		public delegate void ClickDelegate(object sender);
		public event ClickDelegate OnDownEvent;
		public event ClickDelegate OnMoveEvent;
		public event ClickDelegate OnUpEvent;

		public SliderVertical(Vector2 position, float size, Color barColor, Component parent = null, int depth = 0) : base(position, new Vector2(40, size), parent)
        {
            SlidersVertical.Add(this);

            Position = position;
            Size = new Vector2(40, size);

            top = new Image(Vector2.Zero, new Vector2(40, 20), "sliderTop.png", this, false, depth);
            middle = new Image(Vector2.Zero, new Vector2(40, size - 20 * 2), "sliderVertical.png", this, false, depth + 1);
            bottom = new Image(Vector2.Zero, new Vector2(40, 20), "sliderBottom.png", this, false, depth);

            bar = new Image(position + new Vector2(0, 20), new Vector2(40, size - 20 * 2), barColor, this, depth);
            handle = new Image(position, new Vector2(40), "handle.png", this, false, depth);

            BarSize = size - 20 * 2;
            UpdateSliderHeight();

            Value = 0;
        }

        private void UpdateSliderHeight()
        {
            top.Position = Vector2.Zero;
            middle.Position = new Vector2(0, 20);
            bottom.Position = new Vector2(0, 20 + BarSize);
        }

        void UpdateBar()
		{
			if (value < 0)
				value = 0;

			if (value > BarSize)
				value = BarSize;

			bar.Size = new Vector2(40, (float)value);
            bar.Position = new Vector2(bar.Position.X, bar.Position.Y - bar.Size.Y / 2 + (float)value / 2);

            float handlePosY = Position.Y + (float)value;
			handle.Position = new Vector2 (handle.Position.X, handlePosY);
		}

		public static void OnDown(Vector2 position)
		{
			foreach (SliderVertical slider in SlidersVertical) 
			{
				if (slider.Visible) 
				{
                    if(MathHelper.IsPointInArea(position, slider.bar.GlobalPosition, new Vector2(40, slider.BarSize)) || MathHelper.IsPointInArea(position, slider.handle.GlobalPosition, slider.handle.Size))
					{
                        DraggedCount++;
						slider.dragged = true;
                        slider.Value = position.Y - slider.bar.GlobalPosition.Y;

                        if(slider.OnDownEvent != null)
                            slider.OnDownEvent(slider);
						break;
					}
				}
			}
		}

		public static void OnMove(Vector2 position)
		{
			foreach (SliderVertical slider in SlidersVertical) 
			{
				if (slider.Visible) 
				{
					if (slider.dragged) 
					{
                        slider.Value = position.Y - slider.bar.GlobalPosition.Y;

                        if(slider.OnMoveEvent != null)
						    slider.OnMoveEvent(slider);
						break;
					}
				}
			}
		}

		public static void OnUp(Vector2 position)
		{
			foreach (SliderVertical slider in SlidersVertical) 
			{
				if (slider.Visible) 
				{
					if (slider.dragged) 
					{
                        DraggedCount--;
						slider.dragged = false;

                        if(slider.OnUpEvent != null)
						    slider.OnUpEvent (slider);
						break;
					}
				}
			}
		}
	}
}

