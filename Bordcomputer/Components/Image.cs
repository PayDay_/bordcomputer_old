﻿using OpenTK;
using System.Drawing;
using System.Collections.Generic;

namespace Bordcomputer
{
    public class Image : Component
    {
		public static List<Image> Images = new List<Image>();

        public Drawable Drawable;

        private string texturePath;
        public string TexturePath
        {
            get { return texturePath; }
			set 
			{ 
				texturePath = value; 
				Drawable.Texture = Texture.LoadTexture(value, nearestFiltering); 
				if(texturePath.EndsWith(".png"))
					Drawable.Blending = true;
			}
        }

        private bool nearestFiltering = false;

        public override Vector2 Position
        {
            get { return base.Position; }
            set { base.Position = value; }
        }
        public override Vector2 Size
        {
            get { return base.Size; }
            set { base.Size = value; if (Drawable != null) Drawable.Size = value; }
        }
        public override float Rotation
        {
            get { return base.Rotation; }
            set { base.Rotation = value; if (Drawable != null) Drawable.Rotation = value; }
        }
        public override bool Visible
        {
            get { return base.Visible; }
            set { base.Visible = value; if (Drawable != null) Drawable.Visible = value; }
        }

        public Image(Vector2 position, Vector2 size, string path, Component parent = null, bool nearestFiltering = false, int depth = 0) : base(position, size, parent)
        {
            Images.Add(this);
            this.nearestFiltering = nearestFiltering;

            Drawable = new Drawable(Vector2.Zero, size, this, depth);
            TexturePath = path;
        }
        public Image (Vector2 position, Vector2 size, Color color, Component parent = null, int depth = 0) : base(position, size, parent)
		{
			Images.Add (this);

            Drawable = new Drawable(Vector2.Zero, size, this, depth);
            Drawable.Color = color;
            Drawable.OriginalColor = color;
        }
        public Image(Vector2 position, Vector2 size, string path, RectangleF clipping, Component parent = null) : base(position, size, parent)
        {
            Images.Add(this);

            Drawable = new Drawable(Vector2.Zero, size, this);
            TexturePath = path;
            Drawable.Clipping = new Vector4(clipping.X, (Program.SCREEN_HEIGHT - clipping.Y) - clipping.Height, clipping.Width, clipping.Height);
        }
        public Image (Vector2 position, Vector2 size, Color color, RectangleF clipping, Component parent = null) : base(position, size, parent)
        {
            Images.Add (this);

            Drawable = new Drawable(Vector2.Zero, size, this);
            Drawable.Color = color;
            Drawable.OriginalColor = color;
            Drawable.Clipping = new Vector4(clipping.X, (Program.SCREEN_HEIGHT - clipping.Y) - clipping.Height, clipping.Width, clipping.Height);
        }
        public Image(Vector2 position, Vector2 size, string path) : base(position, size)
        {
            Images.Add(this);

            Drawable = new Drawable(Vector2.Zero, size, this);
            TexturePath = path;
        }
    }
}

