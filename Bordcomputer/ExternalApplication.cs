﻿using System;
using System.IO;
using System.Reflection;

namespace Bordcomputer
{
	public class ExternalApplication : Application
	{
		public ExternalApplication () : base ()
		{
		}

		public static void LoadFromOS()
		{
			int p = (int)Environment.OSVersion.Platform;
			bool isLinux = (p == 4) || (p == 6) || (p == 128);

			if(isLinux)
			{
				string externalAppsLocation = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "External Apps");

				Console.WriteLine("Searching for known apps...");

				string[] files = new string[0];
				if (Directory.Exists (externalAppsLocation))
					files = Directory.GetFiles (externalAppsLocation);
				else
					Directory.CreateDirectory (externalAppsLocation);
			}
			else
			{
				Console.WriteLine("External apps are disabled on non-Linux systems.");
			}
		}

		public override void OnOpen ()
		{
			throw new NotImplementedException ();
		}

		public override void OnKeyDown (OpenTK.Input.KeyboardKeyEventArgs e)
		{
			throw new NotImplementedException ();
		}
	}
}

