﻿using System;
using System.IO.Ports;
using System.Threading;

namespace Bordcomputer
{
    public static class Micra
	{
		static SerialPort port;

		public static float Speed = 0;
		public static float RPM = 0;
		public static float Throttle = 0;
		public static float MAF = 0;
		public static float Consumption = 0;
		public static float Tank = 50;

		const int TIME_BETWEEN_VALUES = 20;
        const int TIME_ON_PROBLEM = 200;

		const float MAX_CONSUMPTION = 30;

		public static void Init()
		{
            //foreach (string port in SerialPort.GetPortNames())
                //Console.WriteLine(port);

            Console.WriteLine("Initializing Micra...");

			port = new SerialPort ();
            //port.PortName = "COMOBDB";
            port.PortName = "/dev/ttyUSB0";
            port.BaudRate = 38400;
			port.ReadTimeout = 5;

			try
			{
				port.Open ();
			}
			catch 
			{
				Console.WriteLine ("Serial port could not be opened.");
			}

			if(port.IsOpen)
			{
				Console.WriteLine ("Serial port '" + port.PortName + "' opened.");

				//Initialisierung
				port.Write("ATZ\r");
				Thread.Sleep(1000);
				Console.WriteLine(port.ReadExisting());

                /*port.Write("ATE0\r");
                Thread.Sleep(100);

                Console.WriteLine(port.ReadExisting());

                port.Write("ATL1\r");
                Thread.Sleep(100);

                Console.WriteLine(port.ReadExisting());

                port.Write("ATH1\r");
                Thread.Sleep(100);

                Console.WriteLine(port.ReadExisting());

                port.Write("E0\r");
                Thread.Sleep(100);

                Console.WriteLine(port.ReadExisting());*/

                port.Write("ATSP0\r");
				Thread.Sleep(500);
                Console.WriteLine(port.ReadExisting());

				new Thread(new ThreadStart(Update)).Start();
		    }
		}

		public static void Update()
		{
			while(port.IsOpen)
			{
                foreach(MicraRequest request in MicraRequest.MicraRequests)
                {
                    bool readSuccessful = false;

                    if(request.TimeSinceUpdate >= request.UpdateDelay)
                    {
                        string requestString = "01" + request.RequestString + "1\r";
                        port.Write(requestString);
                        Console.WriteLine("Requesting \"" + requestString + "\".");

                        Thread.Sleep(TIME_BETWEEN_VALUES);

                        string read = "";
                        try
                        {
                            read = port.ReadExisting();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Thread.Sleep(TIME_ON_PROBLEM);
                        }

                        if (read.Length > 0)
                            Console.WriteLine(read);

						if (read.Contains("STOPPED") || read.Contains("SEARCHING") || read.Contains("NO DATA"))
                        {
                            Thread.Sleep(TIME_ON_PROBLEM);
							continue;
                        }
                        else
                        {
                            readSuccessful = true;
                        }

                        string[] array = read.Split('\r');
                        foreach (string line in array)
                        {
                            ParseAnswer(line);
                        }
                        request.TimeSinceUpdate = 0;

                        if (readSuccessful)
                            continue;
                        else
                            break;
                    }
                }
            }
		}

        public static void ParseAnswer(string answer)
        {
			string[] bytes = answer.Trim().Split(' ');

            if (bytes.Length >= 3)
            {
                if (bytes[0] != "41") //If it's not an answer
                    return;

                int A = Convert.ToInt32(bytes[2], 16);
                int B = 0;

                if (bytes.Length >= 4)
                {
                    B = Convert.ToInt32(bytes[3], 16);
                }

                //Speed
                if (bytes[1] == MicraRequest.Speed.RequestString)
                {
                    Speed = A * 1.1F; // * 1.1 to match the Micra's speedo
                    //Console.WriteLine(Speed);
                }
                //RPM
                else if (bytes[1] == MicraRequest.RPM.RequestString)
                {
                    RPM = (A * 256 + B) / 4;
                }
                //Throttle
                else if (bytes[1] == MicraRequest.Throttle.RequestString)
                {
                    Throttle = (float)100 / 255 * A;
                }
                //MAF
                else if (bytes[1] == MicraRequest.MAF.RequestString)
                {
					MAF = (256 * A + B) / 100;
					float gramsPerSecond = MAF / 14.7F;
					float literPerSecond = gramsPerSecond / 750;
					float kmPerSecond = Speed / 3600;

					float consumption = 30;

					if(kmPerSecond != 0)
						consumption = (literPerSecond / kmPerSecond) * 100;
					if(RPM < 100)
						consumption = 0;

					Consumption = Math.Min(consumption, MAX_CONSUMPTION);

					//Console.WriteLine(Consumption + " l");
                }
                //Tank
                else if (bytes[1] == MicraRequest.Tank.RequestString)
				{
					Tank = (float)(100 / 255) * A;
					Console.WriteLine(Tank);
                }
            }
        }
	}
}