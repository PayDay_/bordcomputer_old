#version 310 es

in highp vec4 vert;

uniform mat4 transform;
uniform vec4 textColor;
uniform vec4 atlasRectangle;

out highp vec2 TexCoords;
out highp vec4 TextColor;

uniform mat4 projection;

void main()
{
    	gl_Position = projection * transform * vec4(vert.xy, 1.0, 1.0);
    
	TexCoords = vert.zw;
	TexCoords.x *= atlasRectangle.z;
	TexCoords.x += atlasRectangle.x;
	//TexCoords.y = 1 - TexCoords.y;
	TexCoords.y *= atlasRectangle.w;
	TexCoords.y += atlasRectangle.y;
	
	TextColor = textColor;
}  