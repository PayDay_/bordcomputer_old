#version 310 es

in highp vec4 vert;
in highp vec4 transform1;
in highp vec4 transform2;
in highp vec4 transform3;
in highp vec4 transform4;
in highp vec4 textColor;
in highp vec4 atlasRectangle;

out highp vec2 TexCoords;
out highp vec4 TextColor;

uniform mat4 projection;

void main()
{
	mat4 transform = mat4(transform1, transform2, transform3, transform4);
	
    gl_Position = projection * transform * vec4(vert.xy, 1.0, 1.0);
    
	TexCoords = vert.zw;
	TexCoords.x *= atlasRectangle.z;
	TexCoords.x += atlasRectangle.x;
	//TexCoords.y = 1 - TexCoords.y;
	TexCoords.y *= atlasRectangle.w;
	TexCoords.y += atlasRectangle.y;
	
	TextColor = textColor;
}  