﻿#version 310 es

in highp vec2 fragTexCoord;
layout(location = 0) out highp vec4 outputColor;

uniform sampler2D maintexture;
uniform bool textured;

uniform highp vec4 color;
uniform bool scissored;
uniform highp vec4 scissorRectangle;

void main()
{
	if(scissored)
	{
		if(gl_FragCoord.x < scissorRectangle.x || gl_FragCoord.x > scissorRectangle.x + scissorRectangle.z)
		{
			discard;
		}
		
		if(gl_FragCoord.y < scissorRectangle.y || gl_FragCoord.y > scissorRectangle.y + scissorRectangle.w)
		{
			discard;
		}
	}

	if(textured)
	{
		highp vec2 flipped_texcoord = vec2(fragTexCoord.x, 1.0 - fragTexCoord.y);
		outputColor = texture(maintexture, flipped_texcoord);
		highp float blue = outputColor.b;
		outputColor.b = outputColor.r;
		outputColor.r = blue;
		outputColor = outputColor * color;
	}
    else
	{
		outputColor = color;
	}
}