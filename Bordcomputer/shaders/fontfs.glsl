#version 310 es

in highp vec2 TexCoords;
in highp vec4 TextColor;

layout(location = 0) out highp vec4 outputColor;

uniform sampler2D text;

void main()
{    
    highp vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, TexCoords).r);
    outputColor = TextColor * sampled;
}  