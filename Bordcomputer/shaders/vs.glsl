﻿#version 310 es

in highp vec2 vert;
in highp vec2 vertTexCoord;

out highp vec2 fragTexCoord;

uniform mat4 projection;
uniform mat4 transform;

void main()
{
	vec4 pos = transform * vec4(vert, 0.0, 1.0);
	gl_Position = projection * pos;

    fragTexCoord = vertTexCoord;
}