﻿using System;
using System.IO.Ports;
using System.Threading;
using System.IO;

namespace Bordcomputer
{	
	public static class Micra
	{
		static SerialPort port;

		public static float speed;
		public static float rpm;
		public static float throttle;
		public static float consumption;
		public static float tank;

		const int TIME_BETWEEN_VALUES = 50;

		public static void Init()
		{
			Console.WriteLine("Initializing Micra...");

			port = new SerialPort ();
			port.PortName = "/dev/ttyUSB0";
			port.BaudRate = 38400;
			port.ReadTimeout = 5;

			try
			{
				port.Open ();
			}
			catch 
			{
				Console.WriteLine ("Serial port could not be opened.");
			}

			if(port.IsOpen)
			{
				Console.WriteLine ("Serial port '" + port.PortName + "' opened.");

				//Initialisierung
				port.Write("ATZ\r");
				Thread.Sleep(2000);
				Console.WriteLine(port.ReadExisting());

				port.Write("ATSP 0\r");
				Thread.Sleep(500);

				Console.WriteLine(port.ReadExisting());

				new Thread(new ThreadStart(Update)).Start();
			}
		}

		public static void Update()
		{
			while(port.IsOpen)
			{
				//Console.WriteLine("Update");
				GetVehicleSpeed();
				//GetEngineRPM();
				//throttle = GetThrottle();
				//runTime = GetEngineRunTimeSinceStart();
				//distanceTraveled = GetDistanceTraveled();

				//Thread.Sleep(100);
			}
		}

		static void Receive()
		{
			while(port.IsOpen)
			{

			}
		}

		public static void GetVehicleSpeed()
		{
			bool readRPM = false;
			port.Write ("01 0D \r");

			Thread.Sleep(TIME_BETWEEN_VALUES);

			string read = "";
			try
			{
				read = port.ReadExisting();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				Thread.Sleep (200);
			}

			//Console.WriteLine(read);

			if(read.Contains("STOPPED") || read.Contains("SEARCHING"))
			{
				Thread.Sleep(200);
				return;
			}
			else
			{
				readRPM = true;
				//port.Write ("01 0C \r");
			}

			string[] array = read.Split('\r');

			foreach(string line in array)
			{
				ParseValues (line);
            }

			if (readRPM) 
			{
				bool readThrottle;
				port.Write ("01 0C \r");
				Thread.Sleep(TIME_BETWEEN_VALUES);
				try
				{
					read = port.ReadExisting();
				}
				catch (Exception e)
				{
					Console.WriteLine(e.Message);
					Thread.Sleep (200);
				}

				//Console.WriteLine(read);

				if(read.Contains("STOPPED") || read.Contains("SEARCHING"))
				{
					Thread.Sleep(200);
					return;
				}
				else
				{
					readThrottle = true;
				}

				array = read.Split ('\r');

				foreach (string line in array) {
					ParseValues (line);
				}
				readRPM = false;

				if (readThrottle) 
				{
					bool readConsumption;
					port.Write ("01 49 \r");
					Thread.Sleep(TIME_BETWEEN_VALUES);
					try
					{
						read = port.ReadExisting();
					}
					catch (Exception e)
					{
						Console.WriteLine(e.Message);
						Thread.Sleep (200);
					}
					array = read.Split ('\r');

					if(read.Contains("STOPPED") || read.Contains("SEARCHING"))
					{
						Thread.Sleep(200);
						return;
					}
					else
					{
						readConsumption = true;
					}

					foreach (string line in array)
					{
						ParseValues (line);
					}
					readThrottle = false;

					if (readConsumption)
					{
						bool readTank;
						port.Write ("01 5E \r");
						Thread.Sleep(TIME_BETWEEN_VALUES);
						try
						{
							read = port.ReadExisting();
						}
						catch (Exception e)
						{
							Console.WriteLine(e.Message);
							Thread.Sleep (200);
						}
						array = read.Split ('\r');

						Console.WriteLine("Consumption: " + read);

						if(read.Contains("STOPPED") || read.Contains("SEARCHING"))
						{
							Thread.Sleep(200);
							return;
						}
						else
						{
							readTank = true;
						}

						foreach (string line in array)
						{
							ParseValues (line);
						}
						readConsumption = false;

						if (readTank)
						{
							//bool readTank;
							port.Write ("01 2F \r");
							Thread.Sleep(TIME_BETWEEN_VALUES);
							try
							{
								read = port.ReadExisting();
							}
							catch (Exception e)
							{
								Console.WriteLine(e.Message);
								Thread.Sleep (200);
							}
							array = read.Split ('\r');

							//Console.WriteLine(read);

							if(read.Contains("STOPPED") || read.Contains("SEARCHING"))
							{
								Thread.Sleep(200);
								return;
							}
							/*else
							{
								readTank = true;
							}*/

							foreach (string line in array)
							{
								ParseValues (line);
							}
							//readConsumption = false;
						}
					}
				}
			}
		}

		static void ParseValues(string line)
		{
			//Console.WriteLine("Parse values: " + line);
			//Console.WriteLine (line);
			if (line.StartsWith ("41 0D")) { //Speed
				string[] bytes = line.Split (' ');
				int A = Convert.ToInt32 (bytes [2], 16);
				byte value = (byte)A;
				speed = value * 1.1f;
				//Console.WriteLine(speed);
			} else if (line.StartsWith ("41 0C") || line.StartsWith("1 0C")) { //RPM
				string[] bytes = line.Split (' ');
				int A = Convert.ToInt32 (bytes [2], 16);
				int B = Convert.ToInt32 (bytes [3], 16);
				int value = (A * 256 + B) / 4;
				rpm = value;
				Console.WriteLine("RPM: " + rpm);
			}
			else if (line.StartsWith ("41 49")) { //Throttle
				string[] bytes = line.Split (' ');
				int A = Convert.ToInt32 (bytes [2], 16);
				int value = A;
				throttle = value;
				//Console.WriteLine (value);
			}
			else if (line.StartsWith ("41 5E")) //Consumption
			{
				string[] bytes = line.Split (' ');
				int A = Convert.ToInt32 (bytes [2], 16);
				int B = Convert.ToInt32 (bytes [3], 16);
				int value = (A * 256 + B) / 20;
				consumption = value;
				//Console.WriteLine (value);
			}
			else if (line.StartsWith ("41 2F")) //Tank
			{
				string[] bytes = line.Split (' ');
				int A = Convert.ToInt32 (bytes [2], 16);
				int value = (100 / 255) * A;
				tank = value;
				//Console.WriteLine (value);
			}
		}

		/*public static int GetEngineRPM()
		{
			if(port.IsOpen)
			{
				port.Write ("01 0C \r");

				string received = Receive();

				if(received == "NOPE")
				{
					return rpm;
				}

				string[] bytes = received.Split(' ');
				int A = Convert.ToInt32(bytes[2], 16);
				int B = Convert.ToInt32(bytes[3], 16);
				int value = (A * 256 + B) / 4;
				return value;
			}
			else
			{
				return 0;
			}
		}

		public static float GetThrottle()
		{
			if(port.IsOpen)
			{
				port.Write ("01 5A \r");

				tries = 0;
				string received = Receive();

				if(received == "NOPE")
				{
					return throttle;
				}

				string[] bytes = received.Split(' ');
				int A = Convert.ToInt32(bytes[2], 16);
				float value = A * 100 / 255;
				return value; //Percent
			}
			else
			{
				return 0;
			}
		}

		public static int GetDistanceTraveled()
		{
			if(port.IsOpen)
			{
				port.Write ("01 31 \r");

				tries = 0;
				string received = Receive();

				if(received == "NOPE")
				{
					return distanceTraveled;
				}

				string[] bytes = received.Split(' ');
				int A = Convert.ToInt32(bytes[2], 16);
				int B = Convert.ToInt32(bytes[3], 16);
				int value = A * 256 + B;
				return value; //Kilometer
			}
			else
			{
				return 0;
			}
		}

		public static int GetEngineRunTimeSinceStart()
		{
			if(port.IsOpen)
			{
				port.Write ("01 1F \r");

				tries = 0;
				string received = Receive();

				if(received == "NOPE")
				{
					return runTime;
				}

				string[] bytes = received.Split(' ');
				int A = Convert.ToInt32(bytes[2], 16);
				int B = Convert.ToInt32(bytes[3], 16);
				int value = A * 256 + B;
				return value; //Seconds
			}
			else
			{
				return 0;
			}
		}*/
	}
}