﻿using System;
using System.Collections.Generic;

namespace Bordcomputer
{
    public static class Phone
    {
        public static List<Contact> Contacts = new List<Contact>();

        public class Contact : IComparable
        {
            public string Name;
            public string Number;

            public Contact(string name, string number)
            {
                Name = name;
                Number = number;

                Contacts.Add(this);
            }

            public static bool NumberExists(string number)
            {
                foreach (Phone.Contact contact in Contacts)
                    if (contact.Number == number)
                        return true;

                return false;
            }

            public int CompareTo(object other)
            {
                if (other is Contact)
                {
                    Contact otherContact = (Contact)other;
                    return this.Name.CompareTo(otherContact.Name);
                }

                return 0;
            }
        }
    }
}

