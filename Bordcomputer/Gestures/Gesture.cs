﻿using OpenTK;
using System.Collections.Generic;

namespace Bordcomputer
{
    public class Gesture
    {
        //--------------------------- STATIC --------------------------//
        public static List<Gesture> Gestures = new List<Gesture>();

        private static SlideRight SlideRight = new SlideRight();
        private static SlideLeft SlideLeft = new SlideLeft();

        public static bool BlockGestures;

        protected static Vector2 downPos;
        protected static bool gestureTriggered;

        public Gesture()
        {
            Gestures.Add(this);
        }

        public static void OnUpdateFrame()
        {
            BlockGestures = false;
        }

        //--------------------------- INSTANCE --------------------------//

        public virtual void OnDown(Vector2 position)
        {
            if (BlockGestures)
                return;

            downPos = position;
        }
        public virtual void OnMove(Vector2 position)
        {
            if (BlockGestures)
                return;
        }
        public virtual void OnUp(Vector2 position)
        {
            if (BlockGestures)
                return;

            gestureTriggered = false;
            downPos = Vector2.Zero;
        }
    }
}
