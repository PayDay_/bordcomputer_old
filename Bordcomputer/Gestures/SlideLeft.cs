﻿using OpenTK;

namespace Bordcomputer
{
    public class SlideLeft : Gesture
    {
        const float SLIDE_DISTANCE = 300;

        public delegate void GestureDelegate(object sender);
        public static event GestureDelegate OnGesture;

        public SlideLeft() : base() {  }

        public override void OnDown(Vector2 position)
        {
            base.OnDown(position);
        }
        public override void OnMove(Vector2 position)
        {
            base.OnMove(position);

            if (downPos == Vector2.Zero)
                return;

            if (gestureTriggered)
                return;

            if (downPos.X - position.X >= SLIDE_DISTANCE)
            {
                OnGesture(this);
                gestureTriggered = true;
            }
        }
        public override void OnUp(Vector2 position)
        {
            base.OnUp(position);
        }
    }
}
