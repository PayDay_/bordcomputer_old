﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Bordcomputer
{
    public class Drawable : Transform
    {
        //Constants
        const int DEPTH_MIN = -10;
        const int DEPTH_MAX = 10;

        public static List<Drawable> Drawables = new List<Drawable>();
        public static bool Invalid;

        public static void Invalidate()
        {
            Invalid = true;
        }

        public static void SortByDepth()
        {
            if (!Invalid)
                return;

            List<Drawable> sortedList = new List<Drawable>();
            for (int i = DEPTH_MIN; i < DEPTH_MAX; i++)
                foreach (Drawable drawable in Drawables)
                    if (drawable.Depth == i)
                    {
                        sortedList.Add(drawable);
                    }

            Drawables = sortedList;

            Invalid = false;
        }

        public Color Color = Color.White;
        public Color OriginalColor = Color.White;
        public int Texture = -1;
		public bool Blending = false;
        private int depth = 0;
        public int Depth
        {
            get { return depth; }
            set { depth = value; Invalidate(); }
        }

        private Vector4 clipping = Vector4.Zero;
        public Vector4 Clipping
        {
            get
            {
                if (clipping != Vector4.Zero)
                    return clipping + new Vector4(GlobalPosition.X, -GlobalPosition.Y, 0, 0);
                else
                    return Vector4.Zero;
            }
            set
            { clipping = value; }
        }

        public Drawable(Vector2 position, Vector2 size, Transform parent = null, int depth = 0) : base(position, size, parent)
        {
            Drawables.Add(this);

            Depth = depth;
        }
    }
}
