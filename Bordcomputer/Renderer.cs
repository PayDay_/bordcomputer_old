﻿#define VSYNC

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.ES30;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Bordcomputer
{
    public static class Renderer
    {
        static Shader shader;
        static Shader fontShader;

        static int VAO;
        static int fontVAO;

        static Label labelDrawCalls;
        private static int drawCalls;

        public static Matrix4 Projection;

        public const double ASPECT_RATIO = (double)Program.SCREEN_WIDTH / Program.SCREEN_HEIGHT;

		private static float GLSLVersion;
		private static bool NoInstancing;

        public static void Init()
        {
			GLSLVersion = 130;

			Console.WriteLine("Renderer: " + GL.GetString(StringName.Renderer));
			Console.WriteLine("Vendor: " + GL.GetString(StringName.Vendor));
			Console.WriteLine("Version: " + GL.GetString(StringName.Version));
			//Console.WriteLine(float.Parse(GL.GetString(StringName.ShadingLanguageVersion)));
			string glslVersionString = GL.GetString(StringName.ShadingLanguageVersion);
			Console.WriteLine(glslVersionString);
			if (glslVersionString.Length > 6)
				glslVersionString = glslVersionString.Substring(0, glslVersionString.IndexOf(' '));

			float.TryParse(glslVersionString, out GLSLVersion);
			//NoInstancing = GLSLVersion < 130f ? true : false;
			NoInstancing = false;

			GL.ClearColor(Color.Gray);

            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
			//GL.ShadeModel (ShadingModel.Flat);

            GL.Enable(EnableCap.CullFace);
            GL.CullFace(CullFaceMode.Back);

			GL.ClearDepth (1);

			GL.DepthMask (false);
			GL.Disable (EnableCap.DepthTest);

			GL.Disable (EnableCap.Dither);

            GraphicsContext.CurrentContext.SwapInterval = 1;

            GL.Viewport(0, 0, Program.SCREEN_WIDTH, Program.SCREEN_HEIGHT);

            // Load shaders from file
            shader = new Shader("vs.glsl", "fs.glsl", true);
			if(!NoInstancing)
            	fontShader = new Shader("fontvs.glsl", "fontfs.glsl", true);
			else
				fontShader = new Shader("fontvs_ni.glsl", "fontfs.glsl", true);

			Console.WriteLine (GL.GetError ());

            Projection = Matrix4.CreateOrthographicOffCenter(0, Program.SCREEN_WIDTH, 0, Program.SCREEN_HEIGHT, -5, 5);

			Console.WriteLine (GL.GetError ());

            GL.UseProgram(shader.ProgramID);
            GL.UniformMatrix4(shader.GetUniform("projection"), false, ref Projection);
            CreateVBO();

			Console.WriteLine (GL.GetError ());

            GL.UseProgram(fontShader.ProgramID);
            GL.UniformMatrix4(fontShader.GetUniform("projection"), false, ref Projection);
            CreateFontVBO();

			Console.WriteLine (GL.GetError ());

            //Debug
            labelDrawCalls = new Label("", new Vector2(5, 20), Color.FromArgb(255, 0, 255, 0), Font.SourceSansPro, 20, null, 0, StringAlignment.Near);
            labelDrawCalls.Visible = false;
        }

        private static void CreateVBO()
        {
            List<Vector2> verts = new List<Vector2>();
            List<Vector2> texcoords = new List<Vector2>();

            verts.Add(new Vector2(1.0F, 1.0F));
            verts.Add(new Vector2(0.0F, 1.0F));
            verts.Add(new Vector2(0.0F, 0.0F));

			verts.Add(new Vector2(0.0F, 0.0F));
			verts.Add(new Vector2(1.0F, 0.0F));
			verts.Add(new Vector2(1.0F, 1.0F));

            VAO = GL.GenVertexArray();
            GL.BindVertexArray(VAO);

            texcoords.Add(new Vector2(1, 1)); 
			texcoords.Add(new Vector2(0, 1)); 
			texcoords.Add(new Vector2(0, 0)); 

			texcoords.Add(new Vector2(0, 0)); 
			texcoords.Add(new Vector2(1, 0)); 
			texcoords.Add(new Vector2 (1, 1));

            shader.EnableVertexAttribArrays();

            GL.BindBuffer(BufferTarget.ArrayBuffer, shader.GetBuffer("vert"));
            GL.BufferData<Vector2>(BufferTarget.ArrayBuffer, (IntPtr)(verts.Count * Vector2.SizeInBytes), verts.ToArray(), BufferUsageHint.StaticDraw);
            GL.VertexAttribPointer(shader.GetAttribute("vert"), 2, VertexAttribPointerType.Float, false, 0, 0);

            GL.BindBuffer(BufferTarget.ArrayBuffer, shader.GetBuffer("vertTexCoord"));
            GL.BufferData<Vector2>(BufferTarget.ArrayBuffer, (IntPtr)(texcoords.Count * Vector2.SizeInBytes), texcoords.ToArray(), BufferUsageHint.StaticDraw);
            GL.VertexAttribPointer(shader.GetAttribute("vertTexCoord"), 2, VertexAttribPointerType.Float, true, 0, 0);

            shader.DisableVertexAttribArrays();

            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        private static void CreateFontVBO()
        {
            List<Vector4> verts = new List<Vector4>();
            verts.Add(new Vector4(1.0F, 1.0F, 1, 1));
            verts.Add(new Vector4(0.0F, 1.0F, 0, 1));
            verts.Add(new Vector4(0.0F, 0.0F, 0, 0));

			verts.Add(new Vector4(0.0F, 0.0F, 0, 0));
			verts.Add(new Vector4(1.0F, 0.0F, 1, 0));
			verts.Add(new Vector4(1.0F, 1.0F, 1, 1));

            fontVAO = GL.GenVertexArray();
            GL.BindVertexArray(fontVAO);

            fontShader.EnableVertexAttribArrays();

            GL.BindBuffer(BufferTarget.ArrayBuffer, fontShader.GetBuffer("vert"));
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(Vector4.SizeInBytes * verts.Count), verts.ToArray(), BufferUsageHint.StaticDraw);
            GL.VertexAttribPointer(fontShader.GetAttribute("vert"), 4, VertexAttribPointerType.Float, false, 0, 0);

			if (!NoInstancing)
			{
				int transformAttribute = fontShader.GetAttribute("transform1");
				GL.BindBuffer(BufferTarget.ArrayBuffer, fontShader.GetBuffer("transform1"));
				GL.VertexAttribPointer(transformAttribute, 4, VertexAttribPointerType.Float, false, 4 * Vector4.SizeInBytes, (IntPtr)0);
				GL.VertexAttribPointer(transformAttribute + 1, 4, VertexAttribPointerType.Float, false, 4 * Vector4.SizeInBytes, (IntPtr)(1 * Vector4.SizeInBytes));
				GL.VertexAttribPointer(transformAttribute + 2, 4, VertexAttribPointerType.Float, false, 4 * Vector4.SizeInBytes, (IntPtr)(2 * Vector4.SizeInBytes));
				GL.VertexAttribPointer(transformAttribute + 3, 4, VertexAttribPointerType.Float, false, 4 * Vector4.SizeInBytes, (IntPtr)(3 * Vector4.SizeInBytes));
				GL.VertexAttribDivisor(transformAttribute, 1);
				GL.VertexAttribDivisor(transformAttribute + 1, 1);
				GL.VertexAttribDivisor(transformAttribute + 2, 1);
				GL.VertexAttribDivisor(transformAttribute + 3, 1);

				GL.BindBuffer(BufferTarget.ArrayBuffer, fontShader.GetBuffer("atlasRectangle"));
				GL.VertexAttribPointer(fontShader.GetAttribute("atlasRectangle"), 4, VertexAttribPointerType.Float, true, 0, 0);
				GL.VertexAttribDivisor(fontShader.GetAttribute("atlasRectangle"), 1);

				GL.BindBuffer(BufferTarget.ArrayBuffer, fontShader.GetBuffer("textColor"));
				GL.VertexAttribPointer(fontShader.GetAttribute("textColor"), 4, VertexAttribPointerType.Float, false, 0, 0);
				GL.VertexAttribDivisor(fontShader.GetAttribute("textColor"), 1);
			}
            fontShader.DisableVertexAttribArrays();

			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        public static void Render()
        {
            drawCalls = 0;
            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.Clear(ClearBufferMask.DepthBufferBit);

            GL.BindVertexArray(VAO);
            GL.UseProgram(shader.ProgramID);
            shader.EnableVertexAttribArrays();

            foreach (Drawable drawable in Drawable.Drawables)
            {
                if (drawable.Visible)
                {
                    Draw(drawable);
                    drawCalls++;
                }
            }

            shader.DisableVertexAttribArrays();

            GL.BindVertexArray(fontVAO);
            GL.UseProgram(fontShader.ProgramID);
            fontShader.EnableVertexAttribArrays();
            GL.Enable(EnableCap.Blend);
            GL.ActiveTexture(TextureUnit.Texture0);

			if (!NoInstancing)
				DrawLabelsInstanced();
			else
				DrawLabels();

            fontShader.DisableVertexAttribArrays();
            GL.BindTexture(TextureTarget.Texture2D, 0);

            labelDrawCalls.Text = "DCs: " + drawCalls;
            Program.mainframe.SwapBuffers();
        }

        private static void Draw(Drawable drawable)
        {
            if (drawable.Texture != -1)
            {
                GL.Uniform1(shader.GetUniform("textured"), 1); //Tell shader to use texture
                GL.BindTexture(TextureTarget.Texture2D, drawable.Texture);

                if (drawable.Blending)
                    GL.Enable(EnableCap.Blend);
                else
                    GL.Disable(EnableCap.Blend);
            }
            else
                GL.Uniform1(shader.GetUniform("textured"), 0); //Tell shader to use color

            if(drawable.Clipping != Vector4.Zero)
            {
                GL.Uniform1(shader.GetUniform("scissored"), 1);
                GL.Uniform4(shader.GetUniform("scissorRectangle"), drawable.Clipping);
            }
            else
                GL.Uniform1(shader.GetUniform("scissored"), 0);

            GL.Uniform4(shader.GetUniform("color"), drawable.Color);
            GL.UniformMatrix4(shader.GetUniform("transform"), false, ref drawable.Matrix);

			GL.DrawArrays(PrimitiveType.Triangles, 0, 6);
        }

        private static void DrawLabelsInstanced()
        {
            foreach (Font font in Font.Fonts)
            {
                List<Label> labels = new List<Label>();
                foreach (Label label in Label.Labels)
                    if(label.Visible)
                        if (label.Font == font)
                            labels.Add(label);

                if (labels.Count == 0)
                    continue;

                int charCount = 0;
                foreach (Label label in labels)
                    charCount += label.Text.Length;

                Matrix4[] matrices = new Matrix4[charCount];
                Vector4[] atlasRectangles = new Vector4[charCount];
                Vector4[] colors = new Vector4[charCount];

                int currentChar = 0;
                foreach (Label label in labels)
                {
                    // Activate corresponding render state	
                    Vector4 color = new Vector4((float)label.Color.R / 255, (float)label.Color.G / 255, (float)label.Color.B / 255, (float)label.Color.A / 255);

                    float scale = (float)label.FontSize / label.Font.RenderedSize;

                    float labelWidth = 0;
                    for (int i = 0; i < label.Text.Length; i++)
                        labelWidth += (label.Font.Characters[label.Text[i]].Advance >> 6);
                    labelWidth *= scale;

                    float x = label.GlobalPosition.X;
                    if (label.Alignment == StringAlignment.Center)
                        x -= labelWidth / 2;
                    else if (label.Alignment == StringAlignment.Far)
                        x -= labelWidth;

                    x = (float)Math.Round(x);

                    float y = (float)Program.SCREEN_HEIGHT - label.GlobalPosition.Y;

                    for (int i = 0; i < label.Text.Length; i++)
                    {
			if(font.Characters.Count - 2 < i)
				continue;

                        Font.Character character = label.Font.Characters[label.Text[i]];

                        float xPos = x + character.Bearing.X * scale;
                        float yPos = y - (character.Size.Y - character.Bearing.Y) * scale;

                        float w = character.Size.X * scale;
                        float h = character.Size.Y * scale;

                        Matrix4 matrix = Matrix4.CreateScale(w, h, 1);
                        matrix *= Matrix4.CreateTranslation(xPos, yPos, 0);
                        matrices[currentChar] = matrix;

                        atlasRectangles[currentChar] = character.TexCoords;

                        colors[currentChar] = color;

                        currentChar++;

                        // Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
                        x += (character.Advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64)
                    }
                }

                GL.BindBuffer(BufferTarget.ArrayBuffer, fontShader.GetBuffer("transform1"));
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vector4.SizeInBytes * 4 * charCount), matrices, BufferUsageHint.DynamicDraw);

                GL.BindBuffer(BufferTarget.ArrayBuffer, fontShader.GetBuffer("atlasRectangle"));
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vector4.SizeInBytes * charCount), atlasRectangles, BufferUsageHint.DynamicDraw);

                GL.BindBuffer(BufferTarget.ArrayBuffer, fontShader.GetBuffer("textColor"));
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vector4.SizeInBytes * charCount), colors, BufferUsageHint.DynamicDraw);

                GL.BindTexture(TextureTarget.Texture2D, font.Texture);

                // Render quad
                //GL.Ext.DrawArraysInstanced(PrimitiveType.Quads, 0, 4, charCount);
				GL.DrawArraysInstanced(PrimitiveType.Triangles, 0, 6, charCount);
                drawCalls++;
            }
        }

		private static void DrawLabels()
		{
			foreach (Font font in Font.Fonts)
			{
				List<Label> labels = new List<Label>();
				foreach (Label label in Label.Labels)
					if(label.Visible)
					if (label.Font == font)
						labels.Add(label);

				if (labels.Count == 0)
					continue;

				GL.BindTexture(TextureTarget.Texture2D, font.Texture);

				foreach (Label label in labels)
				{
					// Activate corresponding render state	
					Vector4 color = new Vector4((float)label.Color.R / 255, (float)label.Color.G / 255, (float)label.Color.B / 255, (float)label.Color.A / 255);
                    GL.Uniform4(fontShader.GetUniform("textColor"), ref color);

					float scale = (float)label.FontSize / label.Font.RenderedSize;

					float labelWidth = 0;
					for (int i = 0; i < label.Text.Length; i++)
						labelWidth += (label.Font.Characters[label.Text[i]].Advance >> 6);
					labelWidth *= scale;

					float x = label.GlobalPosition.X;
					if (label.Alignment == StringAlignment.Center)
						x -= labelWidth / 2;
					else if (label.Alignment == StringAlignment.Far)
						x -= labelWidth;

					x = (float)Math.Round(x);

					float y = (float)Program.SCREEN_HEIGHT - label.GlobalPosition.Y;

					for (int i = 0; i < label.Text.Length; i++)
					{
						Font.Character character = label.Font.Characters[label.Text[i]];

						float xPos = x + character.Bearing.X * scale;
						float yPos = y - (character.Size.Y - character.Bearing.Y) * scale;

						float w = character.Size.X * scale;
						float h = character.Size.Y * scale;

						Matrix4 matrix = Matrix4.CreateScale(w, h, 1);
						matrix *= Matrix4.CreateTranslation(xPos, yPos, 0);

						GL.UniformMatrix4(fontShader.GetUniform("transform"), false, ref matrix);
						GL.Uniform4(fontShader.GetUniform("atlasRectangle"), ref character.TexCoords);

						// Render quad
						GL.DrawArrays(PrimitiveType.Quads, 0, 4);
						drawCalls++;

						// Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
						x += (character.Advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64)
					}
				}
			}
		}
    }
}
