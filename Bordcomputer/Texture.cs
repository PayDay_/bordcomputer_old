using OpenTK.Graphics.ES30;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Collections.Generic;

namespace Bordcomputer
{
    public class Texture
	{
        public static Dictionary<string, int> Textures = new Dictionary<string, int>();

		public static int LoadTexture(string path, bool nearestFiltering = false)
		{
            if (path.Length <= 0)
                return -1;

            if(Textures.ContainsKey(path))
            {
                return Textures[path];
            }

			if (!File.Exists("Content/" + path))
			{
				throw new FileNotFoundException("File not found at 'Content/" + path + "'");
			}

			System.Drawing.Imaging.PixelFormat bmpFormat = System.Drawing.Imaging.PixelFormat.Format24bppRgb;
			OpenTK.Graphics.ES30.PixelFormat glFormat = OpenTK.Graphics.ES30.PixelFormat.Rgb;
			TextureComponentCount glIntFormat = TextureComponentCount.Rgb;
			if(path.EndsWith(".png"))
			{
				bmpFormat = System.Drawing.Imaging.PixelFormat.Format32bppArgb ;
				glFormat = OpenTK.Graphics.ES30.PixelFormat.Rgba;
				glIntFormat = TextureComponentCount.Rgba;
			}

			int id = GL.GenTexture();
			GL.BindTexture(TextureTarget.Texture2D, id);
			
			Bitmap bmp = new Bitmap ("Content/" + path);
			BitmapData data = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, bmpFormat);
			GL.TexImage2D(TextureTarget2d.Texture2D, 0, glIntFormat, data.Width, data.Height, 0, glFormat, PixelType.UnsignedByte, data.Scan0);
			
			bmp.UnlockBits(data);
			
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);

            if (nearestFiltering)
            {
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
            }
            else
            {
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            }

            Textures.Add(path, id);
			return id;
		}
	}
}

