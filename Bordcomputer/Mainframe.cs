﻿#define VSYNC

using System;
using OpenTK;
using System.Drawing;
using OpenTK.Input;
using System.Threading;

namespace Bordcomputer
{
    public class Mainframe : GameWindow
	{
        public static Color defaultColor;
		public static Color red;

        private static DateTime lastPrint;

		#region Apps
		public static Launcher AppLauncher;
		public static MusicApp AppMusic;
		public static MonitorApp AppMonitor;
        public static PhoneApp AppPhone;
        public static BluetoothApp AppBluetooth;
		#endregion

		public Mainframe(int Width, int Height) : 
		base(Width, Height, OpenTK.Graphics.GraphicsMode.Default, "Nissan Micra Boardcomputer", GameWindowFlags.Default, DisplayDevice.Default, 3, 1, OpenTK.Graphics.GraphicsContextFlags.Embedded)
			//base(Width, Height)
		{
			this.Title = "Nissan Micra Boardcomputer";
            Font.LoadFonts();
            Renderer.Init();
        }

		protected override void OnLoad (EventArgs e)
		{
			base.OnLoad (e);
            //this.WindowState = WindowState.Fullscreen;
			//Cursor = MouseCursor.Empty;

            Program.DeltaCounter.Start();

			VSync = VSyncMode.On;

            defaultColor = Color.FromArgb (255, 61, 61, 61);
			red = Color.FromArgb (255, 234, 118, 107);

            /*defaultFont = new Font ("Source Sans Pro", 20, FontStyle.Regular);
			largeLightFont = new Font("Source Sans Pro ExtraLight", 22, FontStyle.Regular);
			middleFont = new Font ("Source Sans Pro", 16, FontStyle.Regular);
			smallFont = new Font ("Source Sans Pro Light", 12, FontStyle.Regular);
            titleFont = new Font("Source Sans Pro Light", 26, FontStyle.Regular);*/

            new MusicApp();
            new MonitorApp();
            new PhoneApp();
            new BluetoothApp();
			ExternalApplication.LoadFromOS();

            foreach (Application app in Application.Applications)
                app.Visible = false;

            AppLauncher = new Launcher ();
            AppLauncher.Visible = true;
            AppLauncher.Root.Position = new Vector2(0, 0);
        }

		protected override void OnUpdateFrame (FrameEventArgs e)
		{
            //For frame-independent stuff
            Program.DeltaCounter.Stop();
            Program.ElapsedSeconds = Program.DeltaCounter.Elapsed.TotalSeconds;
            Program.ElapsedMilliseconds = Program.DeltaCounter.Elapsed.TotalMilliseconds;
            Program.DeltaCounter.Reset();
            Program.DeltaCounter.Start();

            if (DateTime.Now.Subtract(lastPrint).TotalMilliseconds >= 200)
            {
                //Console.WriteLine(Program.ElapsedMilliseconds + " ms");
                lastPrint = DateTime.Now;
            }

            base.OnUpdateFrame(e);

			foreach (Application application in Application.Applications)
				application.OnUpdateFrame();
			foreach (Component component in Component.Components)
				component.Update();

            Gesture.OnUpdateFrame();
            SongList.Update();

			MicraRequest.Update();
            
            Transform.UpdateTransforms();
            Drawable.SortByDepth();
        }

		protected override void OnRenderFrame (FrameEventArgs e)
		{
            base.OnRenderFrame (e);
            Renderer.Render();
        }

		protected override void OnMouseDown (OpenTK.Input.MouseButtonEventArgs e)
		{
			base.OnMouseDown (e);
            Vector2 position = new Vector2(e.Position.X, e.Position.Y);

            SliderHorizontal.OnDown (position);
			SliderVertical.OnDown (position);
			Button.OnDown (position);
			ScrollList.OnDown (position);

            foreach (Gesture gesture in Gesture.Gestures)
                gesture.OnDown(position);
            foreach (Application app in Application.Applications)
                app.OnDown(position);
		}
	
		protected override void OnMouseMove (OpenTK.Input.MouseMoveEventArgs e)
		{
			base.OnMouseMove (e);
            Vector2 position = new Vector2(e.Position.X, e.Position.Y);

            SliderHorizontal.OnMove (position);
			SliderVertical.OnMove (position);
			ScrollList.OnMove (position);

            foreach (Gesture gesture in Gesture.Gestures)
                gesture.OnMove(position);
            foreach (Application app in Application.Applications)
                app.OnMove(position);
        }

		protected override void OnMouseUp (OpenTK.Input.MouseButtonEventArgs e)
		{
			base.OnMouseUp (e);
            Vector2 position = new Vector2(e.Position.X, e.Position.Y);

            Button.OnUp (position);
			SliderHorizontal.OnUp (position);
			SliderVertical.OnUp (position);
			ScrollList.OnUp (position);

            foreach (Gesture gesture in Gesture.Gestures)
                gesture.OnUp(position);
            foreach (Application app in Application.Applications)
                app.OnUp(position);
        }

        protected override void OnKeyDown(KeyboardKeyEventArgs e)
        {
            base.OnKeyDown(e);

            foreach(Application application in Application.Applications)
            {
                application.OnKeyDown(e);
            }

			if (e.Key == Key.Escape)
				this.Close ();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
		{
			base.OnClosing (e);

			//Close BASS
			Un4seen.Bass.Bass.BASS_StreamFree (MusicPlayer.stream);
			Un4seen.Bass.Bass.BASS_Free ();
		}
			
	}
}

