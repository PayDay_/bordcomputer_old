﻿#define VSYNC

using System.Diagnostics;
using OpenTK;

namespace Bordcomputer
{
    static class Program
	{
		public static Mainframe mainframe;

        //Frame-independence
        public static Stopwatch DeltaCounter = new Stopwatch();
        public static double ElapsedSeconds;
        public static double ElapsedMilliseconds;

        public const int SCREEN_WIDTH = 800;
        public const int SCREEN_HEIGHT = 480;
        public static bool VSYNC = false;

        public static void Main (string[] args)
		{
			ToolkitOptions options = new ToolkitOptions ();
			options.Backend = PlatformBackend.Default;
			Toolkit.Init (options);

			mainframe = new Mainframe (SCREEN_WIDTH, SCREEN_HEIGHT);

            mainframe.Run(60, 60);
		}
	}
}
