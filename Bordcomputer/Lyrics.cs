﻿using System;
using System.Collections.Generic;
using System.Drawing;
using OpenTK;
using System.Linq;

namespace Bordcomputer
{
	public class Lyrics
	{
        //Constants
        const float POS_X = 400;
        const float POS_Y = 65;
        const float SPACE_Y = 30;
        const float TIME_OFFSET = -0.5F;
        const float LINE_TIME = 15F;

        public string title;
		public string artist;

		public Dictionary<float, string> lines = new Dictionary<float, string>();

		public static int currentLineIndex;

		public static bool lerpingLyrics;
		public static float lerpingBlend;

		public static Lyrics currentLyrics;

		public static Label previousLine;
		public static Label currentLine;
		public static Label nextLine;
		public static Label nextLine2;

        static float fadeLerping = 1;
        static bool fading;
        static bool fadeIn;
        static bool visible;

		public Lyrics(string file)
		{
			string[] brackets = file.Split ('[');

			foreach (string bracket in brackets) 
			{
				if (bracket.Length == 0)
					continue;

				string[] dots = bracket.Split (':');

				if (dots.Length == 0)
					continue;
				int parsed = 0;

                if (!int.TryParse(dots[0], out parsed))
                    continue;

                float seconds = 0;
                if(!float.TryParse(dots[1].Split(']')[0].Replace('.', ','), out seconds))
                    continue;

                float time = parsed * 60 + seconds;

                if (bracket.Length < 9)
                    continue;

                string text = bracket.Remove(0, 9);
                text = text.Replace("\r\n", "");

                if (lines.ContainsKey(time))
                {
                    lines[time] += text;
                }
                else
                {
                    lines.Add(time, text);
                }
			}
		}

		//-------------------------------------------------------STATICS-----------------------------------------------------//
		public static void InitLyrics()
		{
			previousLine = new Label("Previous line", new Vector2(POS_X, POS_Y), Color.FromArgb(128, 255, 255, 255), Font.SourceSansPro, 20, Mainframe.AppMusic.Root, 0, StringAlignment.Center);
			currentLine = new Label("Current line", new Vector2(POS_X, POS_Y + SPACE_Y * 1), Color.White, Font.SourceSansPro, 20, Mainframe.AppMusic.Root, 0, StringAlignment.Center);
			nextLine = new Label("Next line", new Vector2(POS_X, POS_Y + SPACE_Y * 2), Color.FromArgb(128, 255, 255, 255), Font.SourceSansPro, 20, Mainframe.AppMusic.Root, 0, StringAlignment.Center);
			nextLine2 = new Label("Next line 2", new Vector2(POS_X, POS_Y + SPACE_Y * 3), Color.FromArgb(0, 255, 255, 255), Font.SourceSansPro, 20, Mainframe.AppMusic.Root, 0, StringAlignment.Center);

            HardFadeOut();
		}

		public static void SetupSong()
		{
            HardFadeOut();
			if (currentLyrics != null) 
			{
				previousLine.Text = "";
				currentLine.Text = "";
				nextLine.Text = currentLyrics.lines.Values.ElementAt (0);
			}
		}

		public static void UpdateLyrics()
		{
			if (currentLyrics != null) 
			{
				if (currentLineIndex < currentLyrics.lines.Count)
                {
                    if (currentLineIndex >= currentLyrics.lines.Keys.Count - 1)
                        return;

					if (currentLyrics.lines.Keys.ElementAt (currentLineIndex + 1) + TIME_OFFSET <= MusicApp.currentTime)
                    {
						currentLineIndex++;

						lerpingLyrics = true;

						if(currentLineIndex > 0)
							currentLine.Text = currentLyrics.lines.Values.ElementAt (currentLineIndex - 1);

						if (currentLineIndex < currentLyrics.lines.Count - 1) {
							nextLine.Text = currentLyrics.lines.Values.ElementAt (currentLineIndex);
						} else {
							nextLine.Text = "";
						}

						if (currentLineIndex < currentLyrics.lines.Count - 2) {
							nextLine2.Text = currentLyrics.lines.Values.ElementAt (currentLineIndex + 1);
						} else {
							nextLine2.Text = "";
						}

                        if(!visible)
                            FadeIn();
                    }
				}
			}
		}

		public static void SeekLyrics()
		{
            if (currentLyrics == null)
                return;

            bool inLine = false;

            for(int i = 0; i < currentLyrics.lines.Keys.Count; i++)
            {
                float lineTime = currentLyrics.lines.Keys.ElementAt(i);
                if (MusicApp.currentTime >= lineTime)
                {
                    if (MusicApp.currentTime < lineTime + LINE_TIME)
                    {
                        inLine = true;
                        currentLineIndex = i;
                        break;
                    }
                }
            }

            if (inLine)
            {
                if (currentLineIndex > 0)
                    previousLine.Text = currentLyrics.lines.Values.ElementAt(currentLineIndex - 1);
                else
                    previousLine.Text = "";

                currentLine.Text = currentLyrics.lines.Values.ElementAt(currentLineIndex);

                if (currentLineIndex < currentLyrics.lines.Count - 1)
                    nextLine.Text = currentLyrics.lines.Values.ElementAt(currentLineIndex + 1);
                else
                    nextLine.Text = "";

                if (currentLineIndex < currentLyrics.lines.Count - 2)
                    nextLine2.Text = currentLyrics.lines.Values.ElementAt(currentLineIndex + 2);
                else
                    nextLine2.Text = "";

                HardFadeIn();
            }
            else
                HardFadeOut();
		}

        static void HardFadeOut()
        {
            previousLine.Position = new Vector2(-700, previousLine.Position.Y);
            currentLine.Position = new Vector2(-700, currentLine.Position.Y);
            nextLine.Position = new Vector2(-700, nextLine.Position.Y);
            nextLine2.Position = new Vector2(-700, nextLine2.Position.Y);
            fading = false;
            fadeLerping = 0;
            fadeIn = false;
            visible = false;
            SetLabelsVisible(false);
        }

        static void HardFadeIn()
        {
            previousLine.Position = new Vector2(POS_X, previousLine.Position.Y);
            currentLine.Position = new Vector2(POS_X, currentLine.Position.Y);
            nextLine.Position = new Vector2(POS_X, nextLine.Position.Y);
            nextLine2.Position = new Vector2(POS_X, nextLine2.Position.Y);
            fading = false;
            fadeLerping = 0;
            fadeIn = false;
            visible = true;
            SetLabelsVisible(true);
        }

        static void FadeOut(float delay = 0)
        {
            fading = true;
            fadeIn = false;
            fadeLerping = 0;
            Visualisation.FadeIn();
            visible = false;
        }

        static void FadeIn(float delay = 0)
        {
            fading = true;
            fadeIn = true;
            fadeLerping = 0;
            Visualisation.FadeOut();
            visible = true;
            SetLabelsVisible(true);
        }

        public static void Fade()
        {
            if (fading)
            {
                fadeLerping += (float)Program.ElapsedSeconds / 2;

                if (fadeLerping >= 1)
                {
                    fading = false;
                    fadeLerping = 1;

                    if (!fadeIn)
                        SetLabelsVisible(false);
                    else
                        SetLabelsVisible(true);
                }

                float posX = POS_X;
                if (fadeIn)
                    posX = MathHelper.SmoothStep(-700, POS_X, fadeLerping, 4);
                else
                    posX = MathHelper.SmoothStep(POS_X, 1300, fadeLerping, 4);

                previousLine.Position = new Vector2(posX, previousLine.Position.Y);
                currentLine.Position = new Vector2(posX, currentLine.Position.Y);
                nextLine.Position = new Vector2(posX, nextLine.Position.Y);
                nextLine2.Position = new Vector2(posX, nextLine2.Position.Y);
            }
        }

        private static void SetLabelsVisible(bool visible)
        {
            previousLine.Visible = visible;
            currentLine.Visible = visible;
            nextLine.Visible = visible;
            nextLine2.Visible = visible;
        }

        public static void ClearLyrics()
		{
			currentLyrics = null;
			currentLineIndex = -1;

			previousLine.Text = "";
			currentLine.Text = "";
			nextLine.Text = "";
        }

		public static void LerpLyrics()
		{
			if(lerpingLyrics)
			{
				lerpingBlend += (float)Program.ElapsedSeconds;

                int exponent = 2;

                float posY = MathHelper.SmoothStep(POS_Y, POS_Y - SPACE_Y * 1, lerpingBlend, exponent);
                previousLine.Position = new Vector2(previousLine.Position.X, posY);

                posY = MathHelper.SmoothStep(POS_Y + SPACE_Y * 1, POS_Y, lerpingBlend, exponent);
                currentLine.Position = new Vector2(currentLine.Position.X, posY);

                posY = MathHelper.SmoothStep(POS_Y + SPACE_Y * 2, POS_Y + SPACE_Y * 1, lerpingBlend, exponent);
                nextLine.Position = new Vector2(nextLine.Position.X, posY);

                posY = MathHelper.SmoothStep(POS_Y + SPACE_Y * 3, POS_Y + SPACE_Y * 2, lerpingBlend, exponent);
                nextLine2.Position = new Vector2(nextLine2.Position.X, posY);

                int alpha = (int)Math.Round(MathHelper.SmoothStep(0, 255, lerpingBlend, exponent));
                alpha = Math.Max(alpha, 0);
                alpha = Math.Min(alpha, 255);

				previousLine.Color = Color.FromArgb(128 - alpha / 2, 255, 255, 255);
				currentLine.Color = Color.FromArgb(255 - alpha / 2, 255, 255, 255);
				nextLine.Color = Color.FromArgb(128 + alpha / 2, 255, 255, 255);
				nextLine2.Color = Color.FromArgb(alpha / 2, 255, 255, 255);

                if (lerpingBlend >= 1)
                {
                    lerpingLyrics = false;
                    previousLine.Position = new Vector2(previousLine.Position.X, POS_Y);
                    currentLine.Position = new Vector2(currentLine.Position.X, POS_Y + SPACE_Y * 1);
                    nextLine.Position = new Vector2(nextLine.Position.X, POS_Y + SPACE_Y * 2);
                    nextLine2.Position = new Vector2(nextLine2.Position.X, POS_Y + SPACE_Y * 3);

                    previousLine.Color = Color.FromArgb(128, 255, 255, 255);
                    currentLine.Color = Color.FromArgb(255, 255, 255, 255);
                    nextLine.Color = Color.FromArgb(128, 255, 255, 255);
                    nextLine2.Color = Color.FromArgb(0, 255, 255, 255);

                    previousLine.Text = currentLine.Text;
                    currentLine.Text = nextLine.Text;
                    nextLine.Text = nextLine2.Text;

                    lerpingBlend = 0;

                    //Visualisation
                    bool fadeOut = true;

                    if (currentLyrics != null)
                    {
                        if (currentLineIndex < currentLyrics.lines.Count - 1)
                        {
                            if (currentLyrics.lines.Keys.ElementAt(currentLineIndex + 1) - MusicApp.currentTime < Visualisation.NO_LYRICS_TIME)
                            {
                                fadeOut = false;
                            }
                        }
                    }

                    if (fadeOut)
                    {
                        FadeOut();
                    }
                }
			}
		}
	}
}

