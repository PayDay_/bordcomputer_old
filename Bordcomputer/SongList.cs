﻿using OpenTK;
using System.Drawing;
using System.Collections.Generic;

namespace Bordcomputer
{
    public static class SongList
	{
		static Dictionary<Button, SongButton> buttonSongs = new Dictionary<Button, SongButton>();

		const float SONG_HEIGHT = 50;
		public static float scrollHeight;

		public static ScrollList songList;
		public static Label labelNoSongsFound;

		//Sliding in/out
		private static bool sliding;
		private static float slidingProgress;
        private static bool slidingIn;

        //Constants
        private static Color currentSongColor = Color.FromArgb(138, 66, 59);

        public static void InitSongList()
		{
			scrollHeight = SONG_HEIGHT * Song.Songs.Count;
			if (scrollHeight <= 480)
				scrollHeight = 0;
			if (scrollHeight >= 480)
				scrollHeight -= 480;

			songList = new ScrollList (new Vector2 (0, 0), new Vector2 (800, 480), scrollHeight, Color.FromArgb(61, 61, 61));
			songList.OnMoveEvent += new ScrollList.ClickDelegate (OnListMove);
            songList.Visible = false;
            FillSongs ();

			labelNoSongsFound = new Label("No songs found.", new Vector2(280, 220), Color.White, Font.SourceSansProLight, 30, songList, 0, StringAlignment.Center);

			if (Song.Songs.Count == 0)
				labelNoSongsFound.Visible = true;
			else
				labelNoSongsFound.Visible = false;

			songList.Slider.OnDownEvent += new SliderVertical.ClickDelegate (OnListSliderDown);
			songList.Slider.OnMoveEvent += new SliderVertical.ClickDelegate (OnListSliderMove);
			songList.Slider.OnUpEvent += new SliderVertical.ClickDelegate (OnListSliderUp);

            SlideRight.OnGesture += new SlideRight.GestureDelegate(OnSlideRight);
        }

        public static void Update()
        {
            UpdateSliding();
        }

		static void FillSongs()
		{
			Color backgroundColor;
			for(int i = 0; i < Song.Songs.Count; i++)
			{
                backgroundColor = (i % 2 == 1) ? Color.FromArgb(75, 75, 75) : Color.FromArgb(61, 61, 61);
				Button button = new Button (new Vector2 (0, i * SONG_HEIGHT), new Vector2 (800, SONG_HEIGHT), backgroundColor,  songList.Panel);
				button.OnButtonClick += new Button.ClickDelegate (OnSongClick);
				songList.Components.Add (button);


				SongButton songButton = new SongButton ();
				songButton.Button = button;
				songButton.Song = Song.Songs [i];
                songButton.Color = backgroundColor;

				buttonSongs [button] = songButton;

				Label songArtist = new Label (Song.Songs[i].artist, new Vector2 (50, 20), Color.White, Font.SourceSansPro, 20, button, 1);
				songButton.Artist = songArtist;

				Label songTitle = new Label (Song.Songs[i].title, new Vector2 (50, 40), Color.White, Font.SourceSansProBold, 20, button, 1);
				songButton.Title = songTitle;
			}
		}

        private static void OnSlideRight(object sender)
        {
            Close();
        }

        public static void UpdateCurrentSong()
		{
			foreach(SongButton songButton in SongButton.songButtons)
			{
                if (songButton.Song == MusicPlayer.currentSong)
                {
                    songButton.Button.Background.Drawable.Color = currentSongColor;
                    songButton.Button.Background.Drawable.OriginalColor = currentSongColor;
                }
                else
                {
                    songButton.Button.Background.Drawable.Color = songButton.Color;
                    songButton.Button.Background.Drawable.OriginalColor = songButton.Color;
                }
            }
		}

		static void OnSongClick(object sender)
		{
			if (!songList.Dragged && !songList.Slider.dragged) 
			{
                Close();
                MusicApp.PlaySong (buttonSongs[(Button)sender].Song);
			}
		}

		static void OnListMove(object sender)
		{
			songList.Value = songList.Slider.Value;
		}

		static void OnListSliderDown(object sender)
		{
			songList.Value = songList.Slider.Value;
		}

		static void OnListSliderMove(object sender)
		{
			songList.Value = songList.Slider.Value;
		}

		static void OnListSliderUp(object sender)
		{
			songList.Value = songList.Slider.Value;
		}

        public static void Open()
        {
            if (songList.Visible)
                return;
            if (sliding)
                return;

            songList.Position = new Vector2(Mainframe.AppMusic.Root.Size.X, songList.Position.Y);
            sliding = true;
            slidingIn = true;

            if (scrollHeight == 0)
                songList.Slider.Visible = false;
            else
                songList.Slider.Visible = true;

			if (Song.Songs.Count == 0)
				labelNoSongsFound.Visible = true;
			else
				labelNoSongsFound.Visible = false;
        }

        public static void Close()
        {
            if (!songList.Visible)
                return;
            if (sliding)
                return;

            Mainframe.AppMusic.Root.Position = new Vector2(-Mainframe.AppMusic.Root.Size.X, Mainframe.AppMusic.Root.Position.Y);
            sliding = true;
        }

		static void UpdateSliding()
		{
			if (!sliding)
				return;

            if (!Mainframe.AppMusic.Visible)
                Mainframe.AppMusic.Visible = true;

            if (!songList.Visible)
                songList.Visible = true;

            float posX = 0;

            if(!slidingIn)
			    posX = MathHelper.SmoothStep(0, songList.Size.X, slidingProgress);
            else
                posX = MathHelper.SmoothStep(songList.Size.X, 0, slidingProgress);

            songList.Position = new Vector2(posX, songList.Position.Y);
			Mainframe.AppMusic.Root.Position = new Vector2(posX - Mainframe.AppMusic.Root.Size.X, Mainframe.AppMusic.Root.Position.Y);

			slidingProgress += (float)Program.ElapsedSeconds * 2;
			if (slidingProgress >= 1)
			{
                if (!slidingIn)
                    songList.Visible = false;
                else
                    Mainframe.AppMusic.Visible = false;

				Mainframe.AppMusic.Root.Position = Vector2.Zero;
				songList.Position = Vector2.Zero;

                slidingIn = false;
				sliding = false;
				slidingProgress = 0;
			}
		}
	}

	class SongButton
	{
		public static List<SongButton> songButtons = new List<SongButton>();

		public Song Song;
		public Button Button;
		public Label Artist;
		public Label Title;
        public Color Color;

		public SongButton()
		{
			songButtons.Add (this);
		}
	}
}

