﻿using System.Collections.Generic;
using System.IO;

namespace Bordcomputer
{
    public class Song
	{
		public static List<Song> Songs = new List<Song>();

        public AudioType audioType;

		public string title;
		public string artist;

		public string path;

		public Lyrics lyrics;

        public static AudioType GetAudioType(string path)
        {
            string extension = Path.GetExtension(path);

            switch(extension)
            {
                case ".wav":
                    return AudioType.WAV;
                case ".mp3":
                    return AudioType.MP3;
                case ".flac":
                    return AudioType.FLAC;
                case ".m4a":
                    return AudioType.M4A;
                default:
                    return AudioType.UNKNOWN;
            }
        }
	}

    public enum AudioType
    {
        UNKNOWN,
        WAV,
        MP3,
        FLAC,
        M4A,
    }
}

