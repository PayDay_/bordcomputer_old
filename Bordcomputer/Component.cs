﻿using OpenTK;
using System.Collections.Generic;

namespace Bordcomputer
{
    public abstract class Component : Transform
    {
		public static List<Component> Components = new List<Component> ();

        public bool Enabled = true;

        public override Vector2 Position
        {
            set { base.Position = value; }
            get { return base.Position; }
        }
        public override Vector2 Size
        {
            set { base.Size = value; }
            get { return base.Size; }
        }
        
		public Component(Vector2 position, Vector2 size, Transform parent = null) : base(position, size, parent)
        {
            Components.Add(this);
        }

        public virtual void Update() {  }
	}
}

