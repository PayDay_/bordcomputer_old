﻿using System.Collections.Generic;

namespace Bordcomputer
{
    public class MicraRequest
	{
		//-------------- STATIC -------------//
		public static List<MicraRequest> MicraRequests = new List<MicraRequest>();
		
		public static MicraRequest Speed = new MicraRequest("0D", 0.3F);
        public static MicraRequest RPM = new MicraRequest("0C", 0.2F);
		public static MicraRequest Throttle = new MicraRequest("11", 0.4F);
		public static MicraRequest MAF = new MicraRequest("10", 1);
		public static MicraRequest Tank = new MicraRequest("2F", 2);

        public static void Update()
		{
			foreach(MicraRequest request in MicraRequests)
			{
				request.TimeSinceUpdate += (float)Program.ElapsedSeconds;
            }
		}

        //-------------- INSTANCE -------------//
        public float TimeSinceUpdate;
		public float UpdateDelay;
		public string RequestString;

		public MicraRequest(string requestString, float updateDelay, bool active = true)
		{
			this.UpdateDelay = updateDelay;
			this.RequestString = requestString;

            if(active)
                MicraRequests.Add(this);
		}
	}
}

