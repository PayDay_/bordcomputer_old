﻿using System;
using System.Collections.Generic;
using System.IO;
using Un4seen.Bass;
using Un4seen.Bass.AddOn.Flac;

namespace Bordcomputer
{
    public class MusicPlayer
	{
		public static int stream;

        static string MUSIC_PATH = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);
        //static string MUSIC_PATH = @"D:\Musik\";
	
		public static List<Lyrics> lyrics = new List<Lyrics> ();

		public static Song currentSong;
		public static int currentSongID;

		public PlayerMode mode;

		public PlayerState State
		{
			get 
			{
                if (!Bluetooth.Connected)
                {
                    BASSActive active = Bass.BASS_ChannelIsActive(stream);

                    switch (active)
                    {
                        case BASSActive.BASS_ACTIVE_STOPPED:
                            return PlayerState.STOPPED;
                        case BASSActive.BASS_ACTIVE_PAUSED:
                            return PlayerState.PAUSED;
                        case BASSActive.BASS_ACTIVE_PLAYING:
                            return PlayerState.PLAYING;
                        default:
                            return PlayerState.UNKNOWN;
                    }
                }
                else
                {
                    return Bluetooth.MediaPlayer.State;
                }
			}
		}

        public string Artist
        {
            get
            {
                if (!Bluetooth.Connected)
                    return currentSong.artist;
                else
                    return Bluetooth.MediaPlayer.TrackArtist;
            }
        }

        public string Title
        {
            get
            {
                if (!Bluetooth.Connected)
                    return currentSong.title;
                else
                    return Bluetooth.MediaPlayer.TrackTitle;
            }
        }

		public MusicPlayer ()
		{
			bool bassInitiated = Bass.BASS_Init (-1, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);

			if (!bassInitiated)
				throw new Exception ("BASS failed to initialize.");

            #region Bluetooth
            Bluetooth.ConnectionSuccessful += (int device) => OnBluetoothConnected(device);
            Bluetooth.Disconnected += (int device) => OnBluetoothDisconnected(device);
            #endregion
		}

        private void OnBluetoothConnected(int device)
        {
            Bass.BASS_ChannelPause(stream);
            Bluetooth.MediaPlayer.Play();
        }

        private void OnBluetoothDisconnected(int device)
        {
            Bluetooth.MediaPlayer.Pause();
        }

		public void Scan()
		{
			Song.Songs.Clear ();

			ScanSongs (MUSIC_PATH);
			AssignLyrics ();
		}
			
		private void ScanSongs( string sDir )
		{
            foreach (string f in Directory.GetFiles(sDir, "*", SearchOption.AllDirectories))
			{
				string[] splitted = f.Split (Path.DirectorySeparatorChar);
				string name = splitted [splitted.Length - 1];

				if(name.EndsWith("flac") || name.EndsWith("wav") || name.EndsWith("mp3") || name.EndsWith("m4a"))
				{
					Song newSong = new Song ();
					newSong.title = "";
					newSong.artist = "";
					newSong.path = f.Remove (0, sDir.Length);
                    newSong.audioType = Song.GetAudioType(newSong.path);

					TagLib.File file = TagLib.File.Create (f);

					if(file.Tag.Title != null)
						newSong.title = file.Tag.Title;
					if (file.Tag.Performers.Length > 0)
						newSong.artist = file.Tag.Performers [0];

					Song.Songs.Add (newSong);

					if(newSong.title.Length <= 0 || newSong.artist.Length <= 0)
					{
						string[] ending = splitted [splitted.Length - 1].Split ('.');
						name = name.Remove (name.Length - ending[ending.Length - 1].Length - 1);
						string[] nameSplitted = name.Split ('-');

						if (nameSplitted.Length >= 2) 
						{
							newSong.title = nameSplitted [1].Trim ();
							newSong.artist = nameSplitted [0].Trim ();
						}
						else
						{
							//Console.WriteLine ("Could not find title and/or artist for '" + name + "'.");
							newSong.title = name;
							//newSong.artist = "Unknown";
						}
					}
				}
				else if(name.EndsWith("lrc"))
				{
					string[] ending = splitted [splitted.Length - 1].Split ('.');
					name = name.Remove (name.Length - ending[ending.Length - 1].Length - 1);

					string lyricsFile = File.ReadAllText (f);

					Lyrics newLyrics = new Lyrics (lyricsFile);

					string[] lyricsFileSplitted = lyricsFile.Split('[');
					foreach(string split in lyricsFileSplitted)
					{
						if(split.StartsWith("ti"))
						{
							string title = split.Remove(0, 3);
							title = title.Remove (title.Length - 3);
							newLyrics.title = title;
						}
						else if(split.StartsWith("ar"))
						{
							string artist = split.Remove(0, 3);
							artist = artist.Remove(artist.Length - 3);
							newLyrics.artist = artist;
						}
					}

					lyrics.Add (newLyrics);
				}				

			}
		}

		void AssignLyrics()
		{
			foreach (Lyrics lyric in lyrics) 
			{
				int lastDistance = lyric.title.Length;

				Song bestSong = null;

				foreach (Song song in Song.Songs) 
				{
					int distance = MathHelper.EditDistance (song.title, lyric.title);

					if(distance < lastDistance)
					{
						lastDistance = distance;
						bestSong = song;
					}
				}

				if(lastDistance < lyric.title.Length / 2)
				{
					bestSong.lyrics = lyric;
					//Console.WriteLine (bestSong.title + " = " + lyric.title);
				}
				else
				{
					//Console.WriteLine ("No song for '" + lyric.title + "' found.");
				}
			}
		}

		public void Play()
		{
            if (!Bluetooth.Connected)
            {
                if (State != PlayerState.PLAYING)
                {
                    Bass.BASS_ChannelPlay(stream, false);
                }
            }
            else
            {
                Bluetooth.MediaPlayer.Play();
            }
		}

		public void Play(Song song)
		{
			Bass.BASS_StreamFree (stream);
			Lyrics.ClearLyrics();
			string newPath = MUSIC_PATH + song.path;
			currentSongID = Song.Songs.IndexOf(song);

            if (song.audioType == AudioType.FLAC)
            {
                stream = BassFlac.BASS_FLAC_StreamCreateFile(newPath, 0, 0, BASSFlag.BASS_DEFAULT);
            }
            else
            {
                stream = Bass.BASS_StreamCreateFile(newPath, 0, 0, BASSFlag.BASS_DEFAULT);
            }

			Bass.BASS_ChannelPlay (stream, false);

			MusicApp.maxTime = GetSongLength ();
			MusicApp.currentTime = 0;
			MusicApp.slowUpdate = 0;
			currentSong = song;
			Lyrics.currentLyrics = song.lyrics;
			Lyrics.SetupSong ();
		}

		public void Pause()
		{
            if (!Bluetooth.Connected)
                Bass.BASS_ChannelPause(stream);
            else
                Bluetooth.MediaPlayer.Pause();
		}

		public void Next(bool songEnded = false)
		{
            if (!Bluetooth.Connected)
            {
                if (mode == PlayerMode.REPEAT)
                {
                    if (currentSongID < Song.Songs.Count - 1)
                    {
                        currentSongID++;
                    }
                    else
                    {
                        currentSongID = 0;
                    }
                }
                else if (mode == PlayerMode.REPEATSONG)
                {
                    if (!songEnded)
                    {
                        if (currentSongID < Song.Songs.Count - 1)
                        {
                            currentSongID++;
                        }
                        else
                        {
                            currentSongID = 0;
                        }
                    }
                }
                else if (mode == PlayerMode.RANDOM)
                {
                    int randomSong = currentSongID;
                    Random random = new Random();
                    while (randomSong == currentSongID)
                    {
                        randomSong = random.Next(Song.Songs.Count);
                    }
					
                    currentSongID = randomSong;
                }
                Play(Song.Songs[currentSongID]);
            }
            else
                Bluetooth.MediaPlayer.Next();
		}

		public void Previous()
		{
            if (!Bluetooth.Connected)
            {
                if (currentSongID > 0)
                {
                    currentSongID--;
                }
                else
                {
                    currentSongID = Song.Songs.Count - 1;
                }
                Play(Song.Songs[currentSongID]);
            }
            else
                Bluetooth.MediaPlayer.Previous();
		}

		public void Seek(double time)
		{
			Bass.BASS_ChannelSetPosition (stream, time);
		}

		public void SetVolume(float volume)
		{
            Bass.BASS_ChannelSetAttribute(stream, BASSAttribute.BASS_ATTRIB_VOL, volume);
		}

        public double GetPosition()
        {
            if (!Bluetooth.Connected)
            {
                long pos = Bass.BASS_ChannelGetPosition(stream);
                return Bass.BASS_ChannelBytes2Seconds(stream, pos);
            }
            else
            {
                return Bluetooth.MediaPlayer.Position;
            }
        }

        public double GetSongLength()
		{
            if (!Bluetooth.Connected)
            {
                long pos = Bass.BASS_ChannelGetLength(stream);
                return Bass.BASS_ChannelBytes2Seconds(stream, pos);
            }
            else
            {
                return Bluetooth.MediaPlayer.TrackDuration;
            }
		}
	}

	public enum PlayerMode
	{
		REPEAT,
		REPEATSONG,
		RANDOM,
	}

	public enum PlayerState
	{
		UNKNOWN,
		STOPPED,
		PAUSED,
		PLAYING,
	}
}