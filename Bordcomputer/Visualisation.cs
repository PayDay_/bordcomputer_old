﻿using OpenTK;
using System;
using System.Drawing;
using Un4seen.Bass;
using Un4seen.Bass.Misc;

namespace Bordcomputer
{
    public static class Visualisation
    {
        static Visuals visuals = new Visuals();

        static FrequencyBand[] frequencies;
        static Image[] pillars;

		static double update;

        static float fadeMultiplier = 1;
        static bool fading;
        static bool fadeIn;

        static float pillarWidth;
        static int pillarCount = 10;
        static float pillarGap;

        static int inputStream;
        static RECORDPROC recordProc;
        static float[] oldValues;

        //Konstanten
        const float ROOT_Y = 220;
        public const double NO_LYRICS_TIME = 10;
        public const float FADE_SPEED = 1;
        const float PILLAR_HEIGHT = 90;

        public static void Init()
        {
            visuals.ScaleFactorSqr = 6;

            double lastDistance = 16;
            pillarGap = (int)Math.Round(50f / pillarCount);
            pillarGap = Math.Max(pillarGap, 1);
            pillarWidth = (750 - pillarGap * (pillarCount - 1)) / pillarCount;

            frequencies = new FrequencyBand[pillarCount];
            pillars = new Image[pillarCount];
            oldValues = new float[pillarCount];

            for (int i = 0; i < pillarCount; i++)
            {
                int nextI = i + 1;

                int fromFrequency = (int)Math.Round(lastDistance);

                lastDistance *= 1.996F; //10
                //lastDistance *= (Math.Log(20) / Math.E) * 1.282; //20

                int toFrequency = (int)Math.Round(lastDistance);
                frequencies[i] = new FrequencyBand(fromFrequency, toFrequency);

                Color color = Color.FromArgb(235, 44, 25);
                if (i % 2 == 1)
                    color = Color.FromArgb(215, 40, 23);

                pillars[i] = new Image(new Vector2(25 + pillarWidth * i + pillarGap * i, ROOT_Y), new Vector2(pillarWidth, 0), color, Mainframe.AppMusic.Root);

                //Console.WriteLine(fromFrequency + " - " + toFrequency);
            }

            #region Bluetooth
            Bluetooth.ConnectionSuccessful += (int device) => OnBluetoothConnected(device);
            Bluetooth.Disconnected += (int device) => OnBluetoothDisconnected(device);
            #endregion
        }

        private static void OnBluetoothConnected(int device)
        {
            Bass.BASS_RecordFree();

            Bass.BASS_RecordInit(1);
            recordProc = new RECORDPROC(RecordProc);
            inputStream = Bass.BASS_RecordStart(44100, 2, BASSFlag.BASS_SAMPLE_FLOAT, recordProc, IntPtr.Zero);
        }

        private static void OnBluetoothDisconnected(int device)
        {
            Bass.BASS_RecordFree();
        }

        private static bool RecordProc(int handle, IntPtr buffer, int length, IntPtr user)
        {
            return true;
        }

        public static void OnUpdateFrame()
        {
			update += Program.ElapsedSeconds;

			if (update > 0.01) 
			{
                int stream = 0;
                if (!Bluetooth.Connected)
                    stream = MusicPlayer.stream;
                else
                    stream = inputStream;

				for (int i = 0; i < frequencies.Length; i++) 
                {
                    float value = 0;
                    if(!Bluetooth.Connected)
                        value = visuals.DetectFrequency(stream, frequencies[i].min, frequencies[i].max, false);
                    else
                    {
                        float freq = visuals.DetectFrequency(stream, frequencies[i].min, frequencies[i].max, false);
                        value = freq;
                        value = MathHelper.SmoothStepChange(oldValues[i], oldValues[i], freq, (float)Program.ElapsedSeconds, 200);
                        oldValues[i] = value;
                    }
                    float height = value * PILLAR_HEIGHT;
                    height = (float)Math.Round(height);

                    pillars[i].Size = new Vector2 (pillarWidth, height);
                    pillars[i].Position = new Vector2(pillars[i].Position.X, ROOT_Y - height);
                }

				update = 0;
			}

            if(fading)
            {
                if(fadeIn)
                {
                    fadeMultiplier += (float)Program.ElapsedSeconds / FADE_SPEED;

                    if(fadeMultiplier >= 1)
                    {
                        fading = false;
                        fadeMultiplier = 1;
                    }
                }
                else
                {
                    fadeMultiplier -= (float)Program.ElapsedSeconds / FADE_SPEED;

                    if (fadeMultiplier <= 0)
                    {
                        fading = false;
                        fadeMultiplier = 0;
                    }
                }
            }
        }

        public static void FadeOut()
        {
            fading = true;
            fadeIn = false;
        }

        public static void FadeIn()
        {
            fading = true;
            fadeIn = true;
        }
    }

    class FrequencyBand
    {
        public int min;
        public int max;

        public FrequencyBand(int min, int max)
        {
            this.min = min;
            this.max = max;
        }
    }
}
