﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Input;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Specialized;

namespace Bordcomputer
{
    public class BluetoothApp : Application
	{
        const float BUTTON_HEIGHT = 50;

        ScrollList pairedDeviceList;
        PairedDeviceButton[] pairedDeviceButtons = new PairedDeviceButton[10];

        ScrollList discoveredDeviceList;
        DeviceButton[] discoveredDeviceButtons = new DeviceButton[10];

        private float updateTimer = 5;

		private Image scanBackground;
		private Image scanRefresh;
		private Image pairBackground;
		private Label pairingLabel;

		private bool oldPairing;

		//Sliding in/out
		private bool slidingScan;
		private float slidingScanProgress;
		private bool slidingIn;
		private bool slidingToPairing;

		public override void OnLoad ()
		{
            base.OnLoad();

            Icon = "bluetooth.png";
            Name = "Bluetooth";

            Mainframe.AppBluetooth = this;

            Root = new Image(new Vector2(0, 0), new Vector2(800, 50), "topBar.jpg", null, false, 1);
            new Label("Bluetooth", new Vector2(4, 38), Color.White, Font.SourceSansProLight, 40, Root);
            new Label("Paired devices", new Vector2(400, 32), Color.White, Font.SourceSansPro, 25, Root, 0, StringAlignment.Center);
			Button buttonAdd = new Button (new Vector2 (335, 340), new Vector2 (130), "button.png", "add.png", Root, 2);
			buttonAdd.OnButtonClick += new Button.ClickDelegate (OnButtonAddClick);

            new Image(new Vector2(0, 480 - 150), new Vector2(800, 150), "bottomBar.jpg", Root, false, 1);

            scanBackground = new Image (new Vector2 (800, 0), new Vector2 (800, 50), "topBar.jpg", null, false, 1);
			new Label("Bluetooth", new Vector2(4, 38),Color.White, Font.SourceSansProLight, 40, scanBackground, 1);
			new Label("Discovered devices", new Vector2(400, 32), Color.White, Font.SourceSansPro, 25, scanBackground, 1, StringAlignment.Center);

			pairBackground = new Image (new Vector2 (800*2, 0), new Vector2 (800, 480), "backgroundTop.jpg");
			new Label("Bluetooth", new Vector2(4, 38), Color.White, Font.SourceSansProLight, 40, pairBackground);
			new Label("Pairing", new Vector2(400, 32), Color.White, Font.SourceSansPro, 25, pairBackground, 0, StringAlignment.Center);
			new Label ("Trying to pair with", new Vector2 (400, 220),Color.White, Font.SourceSansPro, 30, pairBackground, 0, StringAlignment.Center);
            pairingLabel = new Label ("", new Vector2 (400, 280), Color.White, Font.SourceSansProBold, 30, pairBackground, 0, StringAlignment.Center);
			pairBackground.Visible = false;

			scanRefresh = new Image (new Vector2 (755, 5), new Vector2 (40), "refresh.png", scanBackground, false, 0);
			scanBackground.Visible = false;
			scanRefresh.Visible = false;

            pairedDeviceList = new ScrollList(new Vector2(0, 50), new Vector2(800, 480 - 50 - 150), 0, Mainframe.defaultColor, Root);
            discoveredDeviceList = new ScrollList(new Vector2(0, 50), new Vector2(800, 480 - 50), 0, Mainframe.defaultColor, scanBackground);

            Color backgroundColor;
			for(int i = 0; i < 10; i++)
			{
                float posY = i * BUTTON_HEIGHT;

                backgroundColor = (i % 2 == 1) ? Color.FromArgb(75, 75, 75) : Color.FromArgb(61, 61, 61);
                Button button = new Button (new Vector2 (0, posY), new Vector2 (800, BUTTON_HEIGHT), backgroundColor, discoveredDeviceList.Panel);
                button.OnButtonClick += new Button.ClickDelegate (OnDiscoveredDeviceClick);
                button.Visible = false;

                Label nameLabel = new Label("", new Vector2(55+40, 20), Color.White, Font.SourceSansProBold, 20, button, 1);
                Label addressLabel = new Label("", new Vector2(55+40, 40), Color.FromArgb(150, 150, 150), Font.SourceSansPro, 20, button, 1);

                discoveredDeviceButtons[i] = new DeviceButton();
                discoveredDeviceButtons[i].Button = button;
                discoveredDeviceButtons[i].Name = nameLabel;
                discoveredDeviceButtons[i].Address = addressLabel;

                button = new Button (new Vector2 (0, posY), new Vector2 (800, BUTTON_HEIGHT), backgroundColor, pairedDeviceList.Panel);
                button.OnButtonClick += new Button.ClickDelegate (OnPairedDeviceClick);
                button.Visible = false;

                nameLabel = new Label("", new Vector2(55+40, 20), Color.White, Font.SourceSansProBold, 20, button, 1);
                addressLabel = new Label("", new Vector2(55+40, 40), Color.FromArgb(150, 150, 150), Font.SourceSansPro, 20, button, 1);
                Button removeButton = new Button(new Vector2(740, 5), new Vector2(40), "smallCircle.png", "delete.png", button);
                removeButton.OnButtonClick += new Button.ClickDelegate (OnPairedDeviceDeleteClick);
                Image circle = new Image(new Vector2(7+40, 5), new Vector2(40), "smallCircle.png", button, false, 1);
                Image innerCircle = new Image(new Vector2(7+40, 5), new Vector2(40), "smallCircleInner.png", button, false, 1);
                innerCircle.Visible = false;

                pairedDeviceButtons[i] = new PairedDeviceButton();
                pairedDeviceButtons[i].Button = button;
                pairedDeviceButtons[i].Name = nameLabel;
                pairedDeviceButtons[i].Address = addressLabel;
                pairedDeviceButtons[i].Circle = circle;
                pairedDeviceButtons[i].InnerCircle = innerCircle;
                pairedDeviceButtons[i].RemoveButton = removeButton;
			}
                
			Bluetooth.Init ();
            Bluetooth.Devices.CollectionChanged += (object sender, NotifyCollectionChangedEventArgs e) => DevicesUpdated(sender, e);
            Bluetooth.ConnectionSuccessful += (int device) => OnConnectionSuccessful(device);

            scanRefresh.Visible = true;
        }

		public override void OnOpen()
		{
            Bluetooth.UpdateDevices();
		}
			
        public override void OnUpdateFrame ()
		{
			base.OnUpdateFrame ();

            Bluetooth.Update();
           
			if (!Bluetooth.Pairing && oldPairing)
			{
				slidingToPairing = true;
				slidingIn = false;
			}
			oldPairing = Bluetooth.Pairing;

			if (scanRefresh.Visible) 
				scanRefresh.Rotation -= (float)Program.ElapsedSeconds * 160;

            updateTimer += (float)Program.ElapsedSeconds;
            if(updateTimer >= 5)
            {
                UpdatePairedDevices();
            }

			UpdateSliding ();
        }

		public override void OnKeyDown(KeyboardKeyEventArgs e)
		{
		}

		private void OnButtonAddClick(object sender)
		{
			if (scanBackground.Visible)
				return;
			if (slidingScan)
				return;

            Bluetooth.EnableScanning();
			scanBackground.Position = new Vector2(Root.Size.X, scanBackground.Position.Y);
			slidingScan = true;
			slidingIn = true;
		}

        private void OnDiscoveredDeviceClick(object sender)
        {
            int index = -1;
            for (int i = 0; i < 10; i++)
                if (sender == discoveredDeviceButtons[i].Button)
                {
                    index = i;
                    break;
                }

			slidingToPairing = true;
			slidingIn = true;
			pairingLabel.Text = discoveredDeviceButtons [index].Name.Text;
            Bluetooth.PairWithDevice(index);
        }

        private void OnPairedDeviceClick(object sender)
        {
            int index = -1;
            for (int i = 0; i < 10; i++)
                if (sender == pairedDeviceButtons[i].Button)
                {
                    index = i;
                    break;
                }
                   
            if (!Bluetooth.Devices[index].Connected)
                Bluetooth.ConnectWithDevice(index);
            else
                Bluetooth.DisconnectFromDevice(index);
        }

        private void OnPairedDeviceDeleteClick(object sender)
        {
            int index = -1;
            for (int i = 0; i < 10; i++)
                if (sender == pairedDeviceButtons[i].RemoveButton)
                {
                    index = i;
                    break;
                }

            Bluetooth.RemoveDevice(index);
            UpdatePairedDevices();
        }

        private void OnConnectionSuccessful(int device)
        {
            
        }

		protected override void OnSlideRight(object sender)
		{
            if (!Visible && !scanBackground.Visible)
				return;
			if (sliding)
				return;
			if (scanBackground.Visible) 
			{
				if(slidingScan)
					return;

                Bluetooth.DisableScanning();
				Root.Position = new Vector2(-Root.Size.X, Root.Position.Y);
				slidingScan = true;

				return;
			}

			Mainframe.AppLauncher.Root.Position = new Vector2(-Mainframe.AppLauncher.Root.Size.X, Mainframe.AppLauncher.Root.Position.Y);
			sliding = true;
		}

        private void DevicesUpdated(object sender, NotifyCollectionChangedEventArgs e)
        {
            UpdateDiscoveredDevices();
            UpdatePairedDevices();
        }

        public void UpdateDiscoveredDevices()
        {
            for (int i = 0; i < discoveredDeviceButtons.Length; i++)
            {
                discoveredDeviceButtons[i].Button.Visible = false;
            }

            float scrollHeight = 0;

            for (int i = 0; i < Bluetooth.Devices.Count; i++) 
			{
                if (Bluetooth.Devices[i].Paired)
                    continue;

                if (Bluetooth.Devices.Count - 1 >= i)
                {
                    scrollHeight += BUTTON_HEIGHT;

                    discoveredDeviceButtons[i].Name.Text = Bluetooth.Devices[i].Name;
                    discoveredDeviceButtons[i].Address.Text = Bluetooth.Devices[i].Address;
                    discoveredDeviceButtons[i].Button.Visible = true;
                }
			}

            if (scrollHeight > discoveredDeviceList.Size.Y)
                discoveredDeviceList.ScrollHeight = scrollHeight;
            else
                discoveredDeviceList.ScrollHeight = 0;
        }

        public void UpdatePairedDevices()
        {
            for (int i = 0; i < pairedDeviceButtons.Length; i++)
            {
                pairedDeviceButtons[i].Button.Visible = false;
                pairedDeviceButtons[i].InnerCircle.Visible = false;
            }
                
            float scrollHeight = 0;

            for (int i = 0; i < Bluetooth.Devices.Count; i++)
            {
                if (!Bluetooth.Devices[i].Paired)
                    continue;

                if (Bluetooth.Devices.Count - 1 >= i)
                {
                    scrollHeight += BUTTON_HEIGHT;

                    pairedDeviceButtons[i].Name.Text = Bluetooth.Devices[i].Name;
                    pairedDeviceButtons[i].Address.Text = Bluetooth.Devices[i].Address;
                    pairedDeviceButtons[i].Button.Visible = true;
                    if (Bluetooth.Devices[i].Connected)
                        pairedDeviceButtons[i].InnerCircle.Visible = true;
                }
            }

            if (scrollHeight > pairedDeviceList.Size.Y)
                pairedDeviceList.ScrollHeight = scrollHeight;
            else
                pairedDeviceList.ScrollHeight = 0;
        }

		private void UpdateSliding()
		{
			if (!slidingScan && !slidingToPairing)
				return;

			if (!Root.Visible && !slidingToPairing)
				Root.Visible = true;

			if (!scanBackground.Visible)
				scanBackground.Visible = true;

			float posX = 0;

			if (!slidingIn)
				posX = MathHelper.SmoothStep (0, scanBackground.Size.X, slidingScanProgress);
			else
				posX = MathHelper.SmoothStep (scanBackground.Size.X, 0, slidingScanProgress);

			if (!slidingToPairing) {
				scanBackground.Position = new Vector2 (posX, scanBackground.Position.Y);
				Root.Position = new Vector2 (posX - Root.Size.X, Root.Position.Y);
			} else 
			{
				if (!pairBackground.Visible)
					pairBackground.Visible = true;

				scanBackground.Position = new Vector2 (posX - scanBackground.Size.X, scanBackground.Position.Y);
				pairBackground.Position = new Vector2 (posX, pairBackground.Position.Y);
			}

			slidingScanProgress += (float)Program.ElapsedSeconds * 2;
			if (slidingScanProgress >= 1)
			{
				if (!slidingToPairing) {
					if (!slidingIn)
						scanBackground.Visible = false;
					else
						Root.Visible = false;
				} else {
					if (!slidingIn)
						pairBackground.Visible = false;
					else
						scanBackground.Visible = false;
				}
					

				Root.Position = Vector2.Zero;
				scanBackground.Position = Vector2.Zero;
				pairBackground.Position = Vector2.Zero;

				slidingIn = false;
				slidingScan = false;
				slidingToPairing = false;
				slidingScanProgress = 0;
			}
		}

        public override void OnDown(Vector2 position)
        {
            base.OnDown(position);
        }
        public override void OnMove(Vector2 position)
        {
            base.OnMove(position);
        }
        public override void OnUp(Vector2 position)
        {
            base.OnUp(position);
        }

        class DeviceButton
        {
            public Button Button;
            public Label Name;
            public Label Address;
        }

        class PairedDeviceButton
        {
            public Button Button;
            public Label Name;
            public Label Address;
            public Image Circle;
            public Image InnerCircle;
            public Button RemoveButton;
        }
    }
}

