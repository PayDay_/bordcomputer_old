﻿using System;
using System.Drawing;
using OpenTK;
using System.Linq;

namespace Bordcomputer
{
	public class MusicApp : Application
	{
		static Button playButton;
		static Button pauseButton;

		static Button nextButton;
		static Button prevButton;
		static Button backButton;

		static Button repeatButton;
		static Button repeatSongButton;
		static Button randomButton;

		static Image background;

		static Slider seekSlider;
		static Slider volumeSlider;

		static MusicPlayer player;
		static MusicApp musicApp;

		public static Label songName;
		public static Label songTime;

		public static float currentTime;
		public static int maxTime;

		public static byte volume;
		public static byte maxVolume = 255;

		private static float oneSec;

		private static bool songEnded;

		static Label previousLine;
		static Label currentLine;
		static Label nextLine;

		static byte currentLineIndex;

		public static Lyrics currentLyrics;

		Font lyricsFont;

		public override void OnLoad (EventArgs e, Font font)
		{
			Application.applications.Add (this);

			lyricsFont = new Font ("TeX Gyre Adventor", 15, FontStyle.Bold);

			player = new MusicPlayer();
			player.Scan ();
			player.mode = PlayerMode.Repeat;

			background = new Image (new Vector2 (0, 0), new Vector2 (800, 480), "background.jpg");
			base.images.Add (background);

			playButton = new Button (new Vector2 (325, 305), new Vector2 (150, 150), "button.png", "play.png");
			playButton.OnButtonClick += new Button.ClickDelegate (OnPlay);
			base.buttons.Add (playButton);

			pauseButton = new Button (new Vector2 (325, 305), new Vector2 (150, 150), "button.png", "pause.png");
			pauseButton.OnButtonClick += new Button.ClickDelegate (OnPlay);
			base.buttons.Add (pauseButton);
			pauseButton.Visible = false;

			nextButton = new Button (new Vector2 (625, 305), new Vector2 (150, 150), "button.png", "next.png");
			nextButton.OnButtonClick += new Button.ClickDelegate (OnNext);
			base.buttons.Add (nextButton);

			prevButton = new Button (new Vector2 (25, 305), new Vector2 (150, 150), "button.png", "prev.png");
			prevButton.OnButtonClick += new Button.ClickDelegate (OnPrevious);
			base.buttons.Add (prevButton);

			backButton = new Button (new Vector2 (25, 25), new Vector2 (100, 50), "wideButton.png", "back.png");
			backButton.OnButtonClick += new Button.ClickDelegate (OnBack);
			base.buttons.Add (backButton);

			#region Modes
			repeatButton = new Button (new Vector2 (515, 340), new Vector2 (70, 70), "button.png", "modeRepeat.png");
			repeatButton.OnButtonClick += new Button.ClickDelegate (OnModeChange);
			base.buttons.Add (repeatButton);

			repeatSongButton = new Button (new Vector2 (515, 340), new Vector2 (70, 70), "button.png", "modeRepeatSong.png");
			repeatSongButton.OnButtonClick += new Button.ClickDelegate (OnModeChange);
			base.buttons.Add (repeatSongButton);

			randomButton = new Button (new Vector2 (515, 340), new Vector2 (70, 70), "button.png", "modeRandom.png");
			randomButton.OnButtonClick += new Button.ClickDelegate (OnModeChange);
			base.buttons.Add (randomButton);
			#endregion

			#region Lyrics
			previousLine = new Label("Previous line", new Vector2(200, 50), new Vector2(600, 40), Color.Gray, lyricsFont);
			base.labels.Add(previousLine);
			currentLine = new Label("Current line", new Vector2(200, 90), new Vector2(600, 40), Color.White, lyricsFont);
			base.labels.Add(currentLine);
			nextLine = new Label("Next line", new Vector2(200, 130), new Vector2(600, 40), Color.Gray, lyricsFont);
			base.labels.Add(nextLine);
			#endregion

			musicApp = this;
			songName = new Label ("", new Vector2 (25, 180), new Vector2 (750, 50), Color.White, font);
			base.labels.Add (songName);

			seekSlider = new Slider (new Vector2 (25, 230), new Vector2 (750, 40), "slider.png", 
				new Vector2 (40, 40), "handle.png", new Vector2 (710, 30), "bar.jpg", new Vector2 (20, 5));
			seekSlider.OnUpEvent += new Slider.ClickDelegate (OnSeekSliderUp);
			base.sliders.Add (seekSlider);

			songTime = new Label ("00:00 / 00:00", new Vector2 (610, 180), new Vector2 (200, 50), Color.White, font);
			base.labels.Add (songTime);

			volumeSlider = new Slider (new Vector2 (420, 5), new Vector2 (375, 20), "slider.png", 
				new Vector2 (20, 20), "handle.png", new Vector2 (355, 16), "bar.jpg", new Vector2 (10, 2));
			volumeSlider.OnUpEvent += new Slider.ClickDelegate (OnVolumeSliderUp);
			base.sliders.Add (volumeSlider);

			volume = maxVolume;
			player.SetVolume (volume);
			volumeSlider.Percent = 100;
		}

		public override void OnOpen()
		{
			UpdatePlayPause ();
			UpdateModeButton ();
		}

		public override void OnUpdateFrame (FrameEventArgs e)
		{
			oneSec += (float)e.Time;

			if (oneSec >= 0.2f) 
			{
				UpdateSongTime ();

				if(currentLyrics != null)
					UpdateLyrics ();
				oneSec = 0;
			}
		}

		static void OnPlay(object sender)
		{
			if (player.PlayerState == PlayerState.Idle) 
			{
				player.Play (MusicPlayer.songs [MusicPlayer.currentSong]);
				songName.Text = MusicPlayer.songs [MusicPlayer.currentSong].name;
			} 
			else if (player.PlayerState == PlayerState.Playing) 
			{
				player.Pause ();
			} 
			else if (player.PlayerState == PlayerState.Paused) 
			{
				player.Play ();
			}

			UpdatePlayPause ();
		}

		static void OnNext(object sender)
		{
			player.Next (songEnded);
			songEnded = false;
			songName.Text = MusicPlayer.songs [MusicPlayer.currentSong].name;
			UpdatePlayPause ();
		}

		static void OnPrevious(object sender)
		{
			player.Previous ();
			songName.Text = MusicPlayer.songs [MusicPlayer.currentSong].name;
			UpdatePlayPause ();
		}

		static void OnBack(object sender)
		{
			musicApp.Visible = false;
			Launcher.launcher.Visible = true;
		}

		static void OnSeekSliderUp(object sender)
		{
			currentTime = seekSlider.Percent / 100 * maxTime;
			player.Seek ((int)currentTime);
			UpdateSongTime ();

			if (currentLyrics != null) {
				currentLineIndex = 0;
				byte index = 0;

				foreach (float time in currentLyrics.lines.Keys) {
					if(index < currentLyrics.lines.Count - 1)
					{
						if (currentTime >= index && currentTime < currentLyrics.lines.Keys.ElementAt (index + 1)) 
						{
							currentLineIndex = index;

							if (currentLineIndex >= 1) 
							{
								previousLine.Text = currentLyrics.lines.Values.ElementAt (currentLineIndex - 1);
							} 
							else 
							{
								previousLine.Text = "";
							}
							currentLine.Text = currentLyrics.lines.Values.ElementAt (currentLineIndex);

							if (currentLineIndex < currentLyrics.lines.Count - 1) 
							{
								nextLine.Text = currentLyrics.lines.Values.ElementAt (currentLineIndex + 1);
							} 
							else 
							{
								nextLine.Text = "";
							}
							break;
						}
					}
					index++;
				}
				UpdateLyrics ();
			}
		}

		static void OnVolumeSliderUp(object sender)
		{
			volume = (byte)(volumeSlider.Percent / 100 * maxVolume);
			player.SetVolume (volume);
		}

		static void OnModeChange(object sender)
		{
			if (player.mode == PlayerMode.Repeat) 
			{
				player.mode = PlayerMode.RepeatSong;
			}
			else if (player.mode == PlayerMode.RepeatSong) 
			{
				player.mode = PlayerMode.Random;
			}
			else if (player.mode == PlayerMode.Random) 
			{
				player.mode = PlayerMode.Repeat;
			}

			UpdateModeButton ();
		}

		static void UpdateModeButton()
		{
			if (player.mode == PlayerMode.Repeat) 
			{
				repeatButton.Visible = true;
				repeatSongButton.Visible = false;
				randomButton.Visible = false;
			}
			else if (player.mode == PlayerMode.RepeatSong) 
			{
				repeatButton.Visible = false;
				repeatSongButton.Visible = true;
				randomButton.Visible = false;
			}
			else if (player.mode == PlayerMode.Random) 
			{
				repeatButton.Visible = false;
				repeatSongButton.Visible = false;
				randomButton.Visible = true;
			}
		}

		static void UpdatePlayPause()
		{
			if (player.PlayerState == PlayerState.Idle) 
			{
				playButton.Visible = true;
				pauseButton.Visible = false;
			} 
			else if (player.PlayerState == PlayerState.Playing) 
			{
				playButton.Visible = false;
				pauseButton.Visible = true;
			} 
			else if (player.PlayerState == PlayerState.Paused) 
			{
				playButton.Visible = true;
				pauseButton.Visible = false;
			}
		}

		public static void UpdateSongTime()
		{
			if(maxTime != 0)
			{
				if (player.PlayerState == PlayerState.Playing) 
				{
					currentTime += (float)oneSec;

					#region currentTime
					byte currentMinutes = (byte)(currentTime / 60);
					byte currentSeconds = (byte)(currentTime - currentMinutes * 60);

					string currentMinutesStr = currentMinutes.ToString();
					string currentSecondsStr = currentSeconds.ToString();

					if (currentMinutes < 10)
						currentMinutesStr = "0" + currentMinutes;

					if (currentSeconds < 10)
						currentSecondsStr = "0" + currentSeconds;
					#endregion

					#region maxTime
					byte maxMinutes = (byte)(maxTime / 60);
					byte maxSeconds = (byte)(maxTime - maxMinutes * 60);

					string maxMinutesStr = maxMinutes.ToString();
					string maxSecondsStr = maxSeconds.ToString();

					if (maxMinutes < 10)
						maxMinutesStr = "0" + maxMinutes;

					if (maxSeconds < 10)
						maxSecondsStr = "0" + maxSeconds;
					#endregion

					songTime.Text = currentMinutesStr + ":" + currentSecondsStr + " / " + maxMinutesStr + ":" + maxSecondsStr;

					if (!seekSlider.dragged) 
					{
						seekSlider.Percent = (float)(currentTime / (float)maxTime * (float)100);
					}

					if (currentTime >= maxTime) 
					{
						songEnded = true;
						OnNext (player);
					}
				}
			}
		}

		static void UpdateLyrics()
		{
			if (currentLineIndex < currentLyrics.lines.Count - 1) 
			{
				if (currentLyrics.lines.Keys.ElementAt (currentLineIndex + 1) <= currentTime) {
					currentLineIndex++;

					previousLine.Text = currentLyrics.lines.Values.ElementAt (currentLineIndex - 1);
					currentLine.Text = currentLyrics.lines.Values.ElementAt (currentLineIndex);

					if (currentLineIndex < currentLyrics.lines.Count - 1) 
					{
						nextLine.Text = currentLyrics.lines.Values.ElementAt (currentLineIndex + 1);
					}
					else
					{
						nextLine.Text = "";
					}
				}
			}
		}

		public static void ClearLyrics()
		{
			currentLyrics = null;
			currentLineIndex = 0;

			previousLine.Text = "";
			currentLine.Text = "";
			nextLine.Text = "";
		}
	}
}

