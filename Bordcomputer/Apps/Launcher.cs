﻿using OpenTK;
using System.Collections.Generic;
using OpenTK.Input;
using System.Drawing;

namespace Bordcomputer
{
    public class Launcher : Application
	{
		static Dictionary<Application,Button> appButtons = new Dictionary<Application, Button>();

        //Sliding into app
        private Application appToOpen;

        public override void OnLoad ()
		{
            Applications.Remove(this);
            Root = new Image (new Vector2 (0, 0), new Vector2 (800, 480), "backgroundTop.jpg");
            new Label("Nissan Micra Boardcomputer", new Vector2(400, 38), Color.White, Font.SourceSansProLight, 40, Root, 0, StringAlignment.Center);

            CreateAppButtons();

            Applications.Add(this);
        }

        public override void OnKeyDown(KeyboardKeyEventArgs e) { }

        public override void OnOpen ()
		{
        }

		public override void OnUpdateFrame ()
		{
            UpdateSliding();
		}

        private void CreateAppButtons()
        {
            for (int i = 0; i < Applications.Count; i++)
            {
                float offset = (Applications.Count - 1) * (800 / Applications.Count);
                offset += 150 / 2;
                offset = 800 - offset;
                offset /= Applications.Count;
                offset -= 5;
                float posX = i * (800 / Applications.Count) + offset;
                Button appButton = new Button(new Vector2(posX, 170), new Vector2(150), Applications[i].Icon, Applications[i].Icon, Root);
                appButtons.Add(Applications[i], appButton);
                appButton.OnButtonClick += new Button.ClickDelegate(OnAppClicked);
                appButton.Visible = true;
                new Label(Applications[i].Name, new Vector2(75, 150 + 20), Color.White, Font.SourceSansPro, 20, appButton, 0, StringAlignment.Center);
            }
        }

		void OnAppClicked(object applicationButton)
		{
            if (!Visible)
                return;
            if (sliding)
                return;

            foreach (Application app in appButtons.Keys) 
			{
				if (applicationButton == appButtons [app]) 
				{
                    app.OnOpen();

                    appToOpen = app;
                    sliding = true;
                    slidingProgress = 0;
                }
			}
		}

        private void UpdateSliding()
        {
            if (!sliding)
                return;

            if (!appToOpen.Visible)
                appToOpen.Visible = true;

            float posX = 0;

            posX = MathHelper.SmoothStep(0, -Root.Size.X, slidingProgress);

            Root.Position = new Vector2(posX, Root.Position.Y);
            appToOpen.Root.Position = new Vector2(posX + Root.Size.X, appToOpen.Root.Position.Y);

            slidingProgress += (float)Program.ElapsedSeconds * 2;
            if (slidingProgress >= 1)
            {
                this.Visible = false;

                Root.Position = Vector2.Zero;
                appToOpen.Root.Position = Vector2.Zero;

                sliding = false;
                slidingProgress = 0;
                appToOpen = null;
            }
        }

        public override void OnDown(Vector2 position)
        {
        }
        public override void OnMove(Vector2 position)
        {
        }
        public override void OnUp(Vector2 position)
        {
        }
    }
}

