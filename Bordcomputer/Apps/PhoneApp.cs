﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Input;
using System.Collections.Generic;
using System.Linq;

namespace Bordcomputer
{
    public class PhoneApp : Application
	{
        const float BUTTON_HEIGHT = 50;

        ContactButton[] contactButtons;
        bool imported = false;

        ScrollList contactList;

		public override void OnLoad ()
		{
            base.OnLoad();

            Icon = "phone.png";
            Name = "Phone";

            Mainframe.AppPhone = this;

            Root = new Image(new Vector2(0, 0), new Vector2(800, 50), "topBar.jpg", null, false, 4);
            new Label("Phone", new Vector2(4, 38), Color.White, Font.SourceSansProLight, 40, Root);
            new Label("Contacts", new Vector2(400, 32), Color.White, Font.SourceSansPro, 25, Root, 0, StringAlignment.Center);

            contactList = new ScrollList (new Vector2 (0, 50), new Vector2 (800, 480 - 50), 0, Color.FromArgb(61, 61, 61), Root);
            contactList.OnMoveEvent += new ScrollList.ClickDelegate (OnListMove);
            contactList.Slider.OnDownEvent += new SliderVertical.ClickDelegate (OnListSliderDown);
            contactList.Slider.OnMoveEvent += new SliderVertical.ClickDelegate (OnListSliderMove);
            contactList.Slider.OnUpEvent += new SliderVertical.ClickDelegate (OnListSliderUp);
        }

		public override void OnOpen()
		{
            if (!imported)
            {
                Bluetooth.ImportPhonebook();
                imported = true;

                contactButtons = new ContactButton[Phone.Contacts.Count];
                Color backgroundColor;
                for(int i = 0; i < Phone.Contacts.Count; i++)
                {
                    Phone.Contact contact = Phone.Contacts[i];

                    float posY = i * BUTTON_HEIGHT;

                    backgroundColor = (i % 2 == 1) ? Color.FromArgb(75, 75, 75) : Color.FromArgb(61, 61, 61);
                    Button button = new Button (new Vector2 (0, posY), new Vector2 (800, BUTTON_HEIGHT), backgroundColor, contactList.Panel);
                    button.OnButtonClick += new Button.ClickDelegate (OnContactClick);
                    button.Visible = false;
                    contactList.Components.Add (button);

                    Label nameLabel = new Label(contact.Name, new Vector2(55, 20), Color.White, Font.SourceSansProBold, 20, button, 2);
                    Label numberLabel = new Label(contact.Number, new Vector2(55, 40), Color.FromArgb(150, 150, 150), Font.SourceSansPro, 20, button, 2);

                    contactButtons[i] = new ContactButton();
                    contactButtons[i].Name = nameLabel;
                    contactButtons[i].Number = numberLabel;
                    contactButtons[i].Button = button;
                }

                contactList.ScrollHeight = Phone.Contacts.Count * BUTTON_HEIGHT - (480 - 50);

                //Bluetooth.Dial("1155");
            }
		}
			
        public override void OnUpdateFrame ()
		{
			base.OnUpdateFrame ();
        }

		public override void OnKeyDown(KeyboardKeyEventArgs e)
		{
		}
            
        private void OnContactClick(object sender)
        {
        }
            
        public override void OnDown(Vector2 position)
        {
            base.OnDown(position);
        }
        public override void OnMove(Vector2 position)
        {
            base.OnMove(position);
        }
        public override void OnUp(Vector2 position)
        {
            base.OnUp(position);
        }

        private void OnListMove(object sender)
        {
            contactList.Value = contactList.Slider.Value;
        }

        void OnListSliderDown(object sender)
        {
            contactList.Value = contactList.Slider.Value;
        }

        void OnListSliderMove(object sender)
        {
            contactList.Value = contactList.Slider.Value;
        }

        void OnListSliderUp(object sender)
        {
            contactList.Value = contactList.Slider.Value;
        }

        class ContactButton
        {
            public Button Button;
            public Label Name;
            public Label Number;
        }
    }
}

