﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Input;
using System.Threading;

namespace Bordcomputer
{
	public class MonitorApp : Application
	{
		const float TANK_ANIMATION_SPEED = 80;
		const double THIRD = (double)1 / 3;

		//Left display
		static Image leftCircle;
		static Image rightCircle;

		static Image leftGrey;
		static Image rightGrey;

		static Image tank;
		static Image tankSurface;

		static Image speedBG;
		static Image speed1;
		static Image speed2;
		static Image speed3;
		static Image labelSpeed;

		static Image rpmBG;
		static Image rpm1;
		static Image rpm2;
		static Image rpm3;

		static Image rpmMarkers;
		static Image speedMarkers;

		static Image speedGreyMarkers;

		static Image throttleBG;
		static Image throttle;

		static Image consumptionBG;
		static Image consumption;

		//------------------LERPING

        //Speed
        static float speedSmoothed;
        static float speedOld;

        //RPM
        static float rpmSmoothed;
        static float rpmOld;

        //Throttle
        static float throttleSmoothed;
        static float throttleOld;

        //------------------ CONSTANTS -------------------//
        const float MAX_SPEED = 200;
        const float MAX_RPM = 8000;
        const float MAX_THROTTLE = 100;
		const float MAX_CONSUMPTION = 30;

        public override void OnLoad ()
		{
            base.OnLoad();

            Icon = "monitor.png";
            Name = "Car Monitor";

            Mainframe.AppMonitor = this;

            //Micra.Init ();
            //Simulator.Init ();

            //--------------------------------BACKGROUND----------------------------------//
            Root = new Image(new Vector2(0, 0), new Vector2(800, 480), "StatusApp/background.png", null, false, -1);

            //--------------------------------RIGHT DISPLAY------------------------------------//
            rightGrey = new Image (new Vector2 (460, 140), new Vector2 (270, 270), "StatusApp/greyCircle.png", Root, false, -3);

			tank = new Image (new Vector2 (460, 275), new Vector2 (270, 270), "StatusApp/tank.jpg", Root, false, -2);
			tankSurface = new Image (new Vector2 (190, tank.Position.Y - 26), new Vector2 (540, 27), "StatusApp/tankSurface.png", Root, true, -2);

			rpmMarkers = new Image (new Vector2 (415, 95), new Vector2 (360, 360), "StatusApp/rpmMarkers.png", Root);
			rpmBG = new Image (new Vector2 (415, 95), new Vector2 (360, 360), "StatusApp/speedBG.png", Root);
			rpm3 = new Image(new Vector2 (415, 95), new Vector2 (360, 360), "StatusApp/rpm.png", Root);
			rpm2 = new Image(new Vector2 (415, 95), new Vector2 (360, 360), "StatusApp/rpm.png", Root);
			rpm1 = new Image(new Vector2 (415, 95), new Vector2 (360, 360), "StatusApp/rpm.png", Root);

			consumptionBG = new Image (new Vector2 (415, 95), new Vector2 (360, 360), "StatusApp/throttleBG.png", Root);
            consumption = new Image(new Vector2(415, 95), new Vector2(360, 360), "StatusApp/consumption.png", new Rectangle(180, 180, 180, 180), Root);

			rightCircle = new Image (new Vector2 (410, 90), new Vector2 (370, 370), "StatusApp/leftCircle.png", Root);

			//--------------------------------LEFT DISPLAY------------------------------------//
			leftGrey = new Image (new Vector2 (70, 140), new Vector2 (270, 270), "StatusApp/greyCircle.png", Root);

			speedMarkers = new Image (new Vector2 (25, 95), new Vector2 (360, 360), "StatusApp/speedMarkers.png", Root);
			speedBG = new Image (new Vector2 (25, 95), new Vector2 (360, 360), "StatusApp/speedBG.png", Root);
			speed3 = new Image(new Vector2 (25, 95), new Vector2 (360, 360), "StatusApp/speed.png", Root);
			speed2 = new Image(new Vector2 (25, 95), new Vector2 (360, 360), "StatusApp/speed.png", Root);
			speed1 = new Image(new Vector2 (25, 95), new Vector2 (360, 360), "StatusApp/speed.png", Root);
			speedGreyMarkers = new Image (new Vector2 (20, 90), new Vector2 (370, 370), "StatusApp/speedGreyMarkers.png", Root);

			//labelSpeed = new Label("000", new Vector2(

			throttleBG = new Image (new Vector2 (25, 95), new Vector2 (360, 360), "StatusApp/throttleBG.png", Root);
			throttle = new Image(new Vector2 (25, 95), new Vector2 (360, 360), "StatusApp/throttle.png", new Rectangle(180, 180, 180, 180), Root);
			leftCircle = new Image (new Vector2 (20, 90), new Vector2 (370, 370), "StatusApp/leftCircle.png", Root);
        }

		public override void OnOpen()
		{
		}
			
        public override void OnUpdateFrame ()
		{
            base.OnUpdateFrame();
            //tankLabel.Text = Micra.Throttle + " %";

            if (Visible)
            {
                UpdateSpeed();
                UpdateRPM();
                UpdateThrottle();
                UpdateConsumption();
                UpdateTank();
            }

			Thread.Sleep (1);
        }

		static void UpdateTank ()
		{
			float normalized = Micra.Tank / 100;

			float posY = (434 - Micra.Tank / 100 * 270) - normalized * 12;

			tank.Position = new Vector2 (tank.Position.X, posY);
			tankSurface.Position = new Vector2 (tankSurface.Position.X, posY - 26);

			//Wave animation
			float surfaceX = tankSurface.Position.X;

			surfaceX += (float)Program.ElapsedSeconds * TANK_ANIMATION_SPEED;
			if (surfaceX >= 460)
				surfaceX = 190;

			tankSurface.Position = new Vector2 (surfaceX, tankSurface.Position.Y);
		}

		static void UpdateRPM ()
		{
			float calcRPM = Math.Min(Micra.RPM, MAX_RPM);

            if (calcRPM != rpmOld)
                rpmOld = calcRPM;

			rpmSmoothed = MathHelper.SmoothStepChange(rpmSmoothed, rpmOld, calcRPM, (float)Program.ElapsedSeconds, 8);

            float normalized = rpmSmoothed / 8000;
            float angle = -normalized * 270;

            if (normalized <= THIRD)
            {
                rpm1.Rotation = angle + 90;
                rpm2.Visible = false;
                rpm3.Visible = false;
            }
            else if (normalized > THIRD && normalized <= THIRD * 2)
			{
                rpm1.Rotation = 0;
                rpm2.Visible = true;
                rpm2.Rotation = angle + 90;
                rpm3.Visible = false;
            }
            else if (normalized > THIRD * 2)
            {
                rpm1.Rotation = 0;
                rpm2.Visible = true;
                rpm2.Rotation = -90;
                rpm3.Visible = true;
                rpm3.Rotation = angle + 90;
            }
        }

		static void UpdateSpeed ()
		{
			float calcSpeed = Math.Min(Micra.Speed, MAX_SPEED);

            if (calcSpeed != speedOld)
                speedOld = calcSpeed;

            speedSmoothed = MathHelper.SmoothStepChange(speedSmoothed, speedOld, calcSpeed, (float)Program.ElapsedSeconds, 4);

            float normalized = speedSmoothed / 200;
			float angle = -normalized * 270;

            if (normalized <= THIRD)
            {
                speed1.Rotation = angle + 90;
                speed2.Visible = false;
                speed3.Visible = false;
            }
            else if (normalized > THIRD && normalized <= THIRD * 2)
            {
                speed1.Rotation = 0;
                speed2.Visible = true;
                speed2.Rotation = angle + 90;
                speed3.Visible = false;
            }
            else if (normalized > THIRD * 2)
            {
                speed1.Rotation = 0;
                speed2.Visible = true;
                speed2.Rotation = -90;
                speed3.Visible = true;
                speed3.Rotation = angle + 90;
            }
        }

		static void UpdateThrottle ()
		{
			float calcThrottle = Math.Min(Micra.Throttle, MAX_THROTTLE);

            if (calcThrottle != throttleOld)
                throttleOld = calcThrottle;

            throttleSmoothed = MathHelper.SmoothStepChange(throttleSmoothed, throttleOld, calcThrottle, (float)Program.ElapsedSeconds, 10);

            float normalized = throttleSmoothed / 100;
            float angle = (normalized * 90) - 90;
            throttle.Rotation = angle;
		}

		static void UpdateConsumption()
		{
			float normalized = Micra.Consumption / MAX_CONSUMPTION;
			Console.WriteLine(Micra.Consumption);
			consumption.Rotation = (normalized * 90) - 90;
		}

		public override void OnKeyDown(KeyboardKeyEventArgs e)
		{
		}

        public override void OnDown(Vector2 position)
        {
            base.OnDown(position);
        }
        public override void OnMove(Vector2 position)
        {
            base.OnMove(position);
        }
        public override void OnUp(Vector2 position)
        {
            base.OnUp(position);
        }
    }
}

