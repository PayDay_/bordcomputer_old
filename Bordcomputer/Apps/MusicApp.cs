﻿using System.Drawing;
using OpenTK;
using OpenTK.Input;
using System;

namespace Bordcomputer
{
    public class MusicApp : Application
	{
		public static Button buttonPlayPause;

		static Button nextButton;
		static Button prevButton;

		static Button buttonMode;

		static Button listButton;

		static SliderHorizontal seekSlider;
		static SliderHorizontal volumeSlider;

		public static MusicPlayer player;

		public static Label songTitle;
		public static Label songArtist;

		public static Label songTime;

        static Image bluetoothIcon;
        static Label bluetoothDeviceLabel;

		public static double currentTime;
		public static double maxTime;

		public static float volume;
		public static float maxVolume = 1;

		private static float fastUpdate;
		public static float slowUpdate;

		private static bool songEnded;

		public override void OnLoad ()
		{
            base.OnLoad();

            Icon = "music.png";
            Name = "Music";

            Mainframe.AppMusic = this;

            SlideLeft.OnGesture += new SlideLeft.GestureDelegate(OnSlideLeft);

            player = new MusicPlayer();
			player.Scan ();
			player.mode = PlayerMode.REPEAT;

			Root = new Image (new Vector2 (0, 0), new Vector2 (800, 480), "background.jpg");

            Visualisation.Init();
            SongList.InitSongList();
            Lyrics.InitLyrics ();

			buttonPlayPause = new Button (new Vector2 (335, 340), new Vector2 (130), "button.png", "play.png", Root);
			buttonPlayPause.OnButtonClick += new Button.ClickDelegate (OnPlay);

            nextButton = new Button (new Vector2 (800-130-25, 340), new Vector2 (130), "button.png", "next.png", Root);
			nextButton.OnButtonClick += new Button.ClickDelegate (OnNext);

            prevButton = new Button (new Vector2 (25, 340), new Vector2 (130), "button.png", "prev.png", Root);
			prevButton.OnButtonClick += new Button.ClickDelegate (OnPrevious);

			listButton = new Button (new Vector2 (210, 370), new Vector2 (70), "smallButton.png", "list.png", Root);
			listButton.OnButtonClick += new Button.ClickDelegate (OnList);

			#region Modes
			buttonMode = new Button (new Vector2 (520, 370), new Vector2 (70), "smallButton.png", "modeRepeat.png", Root);
			buttonMode.OnButtonClick += new Button.ClickDelegate (OnModeChange);
			#endregion

			songArtist = new Label ("", new Vector2 (25, 240), Color.White, Font.SourceSansPro, 20, Root);
            songTitle = new Label("", new Vector2(25, 268), Color.White, Font.SourceSansProBold, 30, Root);

            seekSlider = new SliderHorizontal (new Vector2 (25, 280), 750, Color.White, Root);
			seekSlider.OnUpEvent += new SliderHorizontal.ClickDelegate (OnSeekSliderUp);

            songTime = new Label ("00:00 / 00:00", new Vector2 (710, 267), Color.White, Font.SourceSansPro, 22, Root, 0, StringAlignment.Center);

			volumeSlider = new SliderHorizontal (new Vector2 (420, 5), 375, Color.White, Root);
			volumeSlider.OnUpEvent += new SliderHorizontal.ClickDelegate (OnVolumeSliderUp);
            volumeSlider.Visible = false;

			volume = maxVolume;
			player.SetVolume (volume);
			//volumeSlider.Percent = 100;

            #region Bluetooth
            Bluetooth.ConnectionSuccessful += (int device) => OnBluetoothConnected(device);
            Bluetooth.Disconnected += (int device) => OnBluetoothDisconnected(device);

            bluetoothIcon = new Image(new Vector2(765, 5), new Vector2(30), "bluetoothSmall.png", Root);
            bluetoothIcon.Visible = false;

            bluetoothDeviceLabel = new Label("", new Vector2(-6, 20), Color.White, Font.SourceSansProBold, 18, bluetoothIcon, 1, StringAlignment.Far);
            #endregion
		}

		public override void OnOpen()
		{
			UpdatePlayPause ();
			UpdateModeButton ();
		}

		public override void OnUpdateFrame ()
		{
            base.OnUpdateFrame();
			fastUpdate += (float)Program.ElapsedSeconds;
			slowUpdate += (float)Program.ElapsedSeconds;

            currentTime = player.GetPosition();

            if (fastUpdate >= 0.2F) 
			{
                UpdateSongTime();
                Lyrics.UpdateLyrics ();
			
				fastUpdate = 0;
			}

			if (slowUpdate >= 1) 
			{
                if(Bluetooth.Connected)
                {
                    UpdatePlayPause();
                    maxTime = player.GetSongLength();
                    songArtist.Text = player.Artist;
                    songTitle.Text = player.Title;
                }
				slowUpdate = 0;
			}

            
            Lyrics.LerpLyrics ();
            Lyrics.Fade();

            if(Visible && player.State == PlayerState.PLAYING)
                Visualisation.OnUpdateFrame();
        }

        public override void OnKeyDown(KeyboardKeyEventArgs e)
        {
        }

        private static void OnBluetoothConnected(int device)
        {
            bluetoothIcon.Visible = true;
            bluetoothDeviceLabel.Text = Bluetooth.ConnectedDevice;
            buttonMode.Visible = false;
            listButton.Visible = false;
            seekSlider.Enabled = false;
        }

        private static void OnBluetoothDisconnected(int device)
        {
            bluetoothIcon.Visible = false;
            buttonMode.Visible = true;
            listButton.Visible = true;
            UpdateTitle();
            UpdateSongTime();
            seekSlider.Enabled = true;
        }

		static void UpdateTitle()
		{
            if (Bluetooth.Connected)
                return;

            if (Song.Songs.Count - 1 >= MusicPlayer.currentSongID)
            {
                songTitle.Text = Song.Songs[MusicPlayer.currentSongID].title;
                songArtist.Text = Song.Songs[MusicPlayer.currentSongID].artist;
                SongList.UpdateCurrentSong();
            }
            else
            {
                songTitle.Text = "";
                songArtist.Text = "";
            }
		}

		static void OnPlay(object sender)
		{
            if (!Bluetooth.Connected)
            {
                switch (player.State)
                {
                    case PlayerState.STOPPED:
                        player.Play(Song.Songs[MusicPlayer.currentSongID]);
                        UpdateTitle();
                        break;
                    case PlayerState.PLAYING:
                        player.Pause();
                        break;
                    case PlayerState.PAUSED:
                        player.Play();
                        break;
                }
            }
            else
            {
                switch (player.State)
                {
                    case PlayerState.PLAYING:
                        player.Pause();
                        break;
                    case PlayerState.PAUSED:
                        player.Play();
                        break;
                }
            }

            UpdatePlayPause();
		}

		public static void PlaySong(Song song)
		{
			player.Play (song);
			UpdateTitle ();
			UpdatePlayPause ();
			UpdateModeButton ();
		}

		static void OnNext(object sender)
		{
			player.Next (songEnded);
			songEnded = false;
			UpdateTitle ();
			UpdatePlayPause ();
		}

		static void OnPrevious(object sender)
		{
			player.Previous ();
			UpdateTitle ();
			UpdatePlayPause ();
		}

		static void OnBack(object sender)
		{
			Mainframe.AppMusic.Visible = false;
			Mainframe.AppLauncher.Visible = true;
		}

		static void OnList(object sender)
		{
            SongList.Open();
		}

        private void OnSlideLeft(object sender)
        {
            if (!Visible)
                return;
            if (SliderVertical.DraggedCount > 0 || SliderHorizontal.DraggedCount > 0)
                return;
            if (Bluetooth.Connected)
                return;

            SongList.Open();
        }

        static void OnSeekSliderUp(object sender)
		{
			currentTime = seekSlider.Percent * maxTime;
			player.Seek (currentTime);
			slowUpdate = 0;
			UpdateSongTime ();

			Lyrics.SeekLyrics ();
		}

		static void OnVolumeSliderUp(object sender)
		{
			volume = (float)volumeSlider.Percent / 100;
			player.SetVolume (volume);
		}

		static void OnModeChange(object sender)
		{
            switch (player.mode)
            {
                case PlayerMode.REPEAT:
                    player.mode = PlayerMode.REPEATSONG;
                    break;
                case PlayerMode.REPEATSONG:
                    player.mode = PlayerMode.RANDOM;
                    break;
                case PlayerMode.RANDOM:
                    player.mode = PlayerMode.REPEAT;
                    break;
            }

			UpdateModeButton ();
		}

		static void UpdateModeButton()
		{
            switch (player.mode)
            {
                case PlayerMode.REPEAT:
                    buttonMode.Foreground.TexturePath = "modeRepeat.png";
                    break;
                case PlayerMode.REPEATSONG:
                    buttonMode.Foreground.TexturePath = "modeRepeatSong.png";
                    break;
                case PlayerMode.RANDOM:
                    buttonMode.Foreground.TexturePath = "modeRandom.png";
                    break;
            }
		}

		static void UpdatePlayPause()
		{
            switch (player.State)
            {
                case PlayerState.STOPPED:
                    buttonPlayPause.Foreground.TexturePath = "play.png";
                    break;
                case PlayerState.PLAYING:
                    buttonPlayPause.Foreground.TexturePath = "pause.png";
                    break;
                case PlayerState.PAUSED:
                    buttonPlayPause.Foreground.TexturePath = "play.png";
                    break;
            }
		}

		public static void UpdateSongTime()
		{
			if(maxTime != 0)
			{
				if (player.State == PlayerState.PLAYING) 
				{
					#region currentTime
					byte currentMinutes = (byte)(currentTime / 60);
					byte currentSeconds = (byte)(currentTime - currentMinutes * 60);

					string currentMinutesStr = currentMinutes.ToString();
					string currentSecondsStr = currentSeconds.ToString();

					if (currentMinutes < 10)
						currentMinutesStr = "0" + currentMinutes;

					if (currentSeconds < 10)
						currentSecondsStr = "0" + currentSeconds;
					#endregion

					#region maxTime
					byte maxMinutes = (byte)(maxTime / 60);
					byte maxSeconds = (byte)(maxTime - maxMinutes * 60);

					string maxMinutesStr = maxMinutes.ToString();
					string maxSecondsStr = maxSeconds.ToString();

					if (maxMinutes < 10)
						maxMinutesStr = "0" + maxMinutes;

					if (maxSeconds < 10)
						maxSecondsStr = "0" + maxSeconds;
					#endregion

					songTime.Text = currentMinutesStr + ":" + currentSecondsStr + " / " + maxMinutesStr + ":" + maxSecondsStr;

					if (!seekSlider.dragged) 
					{
						seekSlider.Percent = (float)(currentTime / maxTime);
					}

					if (currentTime >= maxTime - 0.2f)
					{
						songEnded = true;
						OnNext (player);
					}
				}
			}
		}

        public override void OnDown(Vector2 position)
        {
            base.OnDown(position);
        }
        public override void OnMove(Vector2 position)
        {
            base.OnMove(position);
        }
        public override void OnUp(Vector2 position)
        {
            base.OnUp(position);
        }
    }
}