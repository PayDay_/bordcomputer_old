﻿using System.Threading;
using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;

using DBus;
using org.bluez;
using org.freedesktop.DBus;
using org.ofono;
using System.Collections.ObjectModel;

namespace Bordcomputer
{
    public static class Bluetooth
    {
		public static ObservableCollection<Device1> Devices = new ObservableCollection<Device1>();

        public delegate void ConnectionSuccessfulEventHandler(int device);
        public static event ConnectionSuccessfulEventHandler ConnectionSuccessful;

        public delegate void DisconnectedEventHandler(int device);
        public static event DisconnectedEventHandler Disconnected;

        private static bool scanning;
        private static float updateTimer;

        public static string ConnectedDevice;
        public static bool Connected;

		public static bool ScanningForDevices;
        public static bool ReadingPairedDevices;
        public static bool Pairing;

        public static int PairTarget;

        static Bus system;
        static ObjectManager manager;
        static ObjectPath adapterPath;
		static Adapter1 adapter;
		static MediaPlayer1 player;
        static Manager ofonoManager;
        static VoiceCallManager voiceCallManager;

        public static void Init()
        {
            int platform = (int)Environment.OSVersion.Platform;
            bool isLinux = (platform == 4) || (platform == 6) || (platform == 128);

            if(!isLinux)
            {
                Console.WriteLine("Bluetooth features are disabled on non-Linux systems.");
                return;
            }

			system = Bus.System;
            manager = system.GetObject<ObjectManager>("org.bluez", ObjectPath.Root);

            ofonoManager = system.GetObject<Manager>("org.ofono", ObjectPath.Root);

            var managedObjects = manager.GetManagedObjects();
            adapterPath = null;
            foreach (var obj in managedObjects.Keys)
            {
                if(managedObjects[obj].ContainsKey("org.bluez.Adapter1"))
                {
                    if (obj.ToString().EndsWith("hci0"))
                    {
                        System.Console.WriteLine("Adapter found at" + obj);
                        adapterPath = obj;
                        break;
                    }
                }
            }

            adapter = system.GetObject<Adapter1>("org.bluez", adapterPath);
            UpdateDevices();
            for(int i = 0; i < Devices.Count; i++)
            {
                if(Devices[i].Connected)
                {
                    ConnectedDevice = Devices[i].Name;
                    OnConnected(i);
                }
            }
        }

        public static void Update()
        {
            updateTimer += (float)Program.ElapsedSeconds;

            if(scanning)
            {
                if(updateTimer >= 1f)
                {
                    updateTimer = 0;
                    UpdateDevices();
                }
            }
        }

		public static void EnableScanning()
		{
            try { adapter.StopDiscovery(); } catch(Exception) {}
            adapter.StartDiscovery ();
            scanning = true;
		}

        public static void UpdateDevices()
        {
            bool[] devicesConnected = new bool[Devices.Count];
            for(int i = 0; i < Devices.Count; i++)
            {
                devicesConnected[i] = Devices[i].Connected;
            }

            Devices.Clear();

            var managedObjects = manager.GetManagedObjects();
            foreach (var obj in managedObjects.Keys)
            {
                if (managedObjects[obj].ContainsKey("org.bluez.Device1"))
                {
                    Device1 device = system.GetObject<Device1>("org.bluez", obj);

                    bool existing = false;
                    foreach(Device1 dev in Bluetooth.Devices)
                        if(dev.Address == device.Address)
                        {
                            existing = true;
                            break;
                        }

                    if(existing)
                        continue;
                    
                    Devices.Add(device);
                }
            }

            for(int i = 0; i < Devices.Count; i++)
            {
                if (devicesConnected.Length - 1 < i)
                    break;

                if(devicesConnected[i] && !Devices[i].Connected)
                {
                    ConnectedDevice = "";
                    Disconnected(i);
                    break;
                }
            }
        }

        public static void DisableScanning()
        {
            try { adapter.StopDiscovery(); } catch(Exception) {}
            scanning = false;
        }

        public static void PairWithDevice(int index)
        {
            Pairing = true;
            new Thread(Pair).Start();
        }

        private static void Pair()
        {
            if (Devices[PairTarget].Paired)
            {
                Pairing = false;
            }

            Devices[PairTarget].Trusted = true;
            try { Devices[PairTarget].Pair(); }
            catch(Exception) {  }

            Pairing = false;
            Devices[PairTarget].Disconnect();
        }

        public static void ConnectWithDevice(int index)
        {
            for (int i = 0; i < Devices.Count; i++)
                Devices[i].Disconnect();

            Devices[index].Connect();

            Thread.Sleep(1000);

            OnConnected(index);
        }

        private static void OnConnected(int index)
        {
            string address = Devices[index].Address.Replace(':', '_');

            var managedObjects = manager.GetManagedObjects();
            foreach (var obj in managedObjects.Keys)
            {
                if (managedObjects[obj].ContainsKey("org.bluez.MediaPlayer1"))
                {
                    if (obj.ToString().Contains(address))
                        player = system.GetObject<MediaPlayer1>("org.bluez", obj);
                }
            }
                
            voiceCallManager = system.GetObject<VoiceCallManager>("org.ofono", new ObjectPath(@"/hfp/org/bluez/hci0/dev_" + address));

            ProcessStartInfo info = new ProcessStartInfo("sudo", @"rfcomm release /dev/rfcomm0");
            Process.Start(info);
            Thread.Sleep(50);
            info = new ProcessStartInfo("sudo", @"rfcomm bind /dev/rfcomm0 " + Devices[index].Address);
            Process.Start(info);

            ConnectedDevice = Devices[index].Name;

            if (ConnectionSuccessful != null)
                ConnectionSuccessful(index);

            Connected = true;
        }

        public static void DisconnectFromDevice(int index)
        {
            ProcessStartInfo info = new ProcessStartInfo("sudo", @"rfcomm release /dev/rfcomm0");
            Process.Start(info);

            Connected = false;

            if (Disconnected != null)
                Disconnected(index);

            ConnectedDevice = "";
            Devices[index].Disconnect();
        }

        public static void RemoveDevice(int index)
        {
            ObjectPath path = new ObjectPath("/org/bluez/hci0/dev_" + Devices[index].Address.Replace(':', '_'));
            Devices.RemoveAt(index);
            adapter.RemoveDevice(path);
            UpdateDevices();
        }

        public static void Dial(string number)
        {
            voiceCallManager.Dial(number, "");
        }

        public static void ImportPhonebook()
        {
            bool success = true;

            SerialPort port = null;
            try
            {
                port = new SerialPort("/dev/rfcomm0", 9600, Parity.None, 8, StopBits.One);
                port.ReadTimeout = 5000;
                port.Open(); 
                port.Write("AT+CPBS=\"ME\"\r");
                Console.WriteLine(port.ReadLine());
                port.Write("AT+CPBS?\r");
                Console.WriteLine(port.ReadLine());
                Console.WriteLine(port.ReadLine());
                string bookInfo = port.ReadLine();
                Console.WriteLine(bookInfo);
                int contactCount = int.Parse(bookInfo.Split(',')[1]);
                Console.WriteLine(port.ReadLine());
                port.Write("AT+CPBR=1," + contactCount + "\r");
                Console.WriteLine(port.ReadLine());
                while(true)
                {
                    string output = "";
                    try
                    {
                        output = port.ReadLine();
                    }
                    catch { success = false; break; }

                    if (output.Length < 20)
                        continue;

                    //Console.WriteLine(output);
                    string[] split = output.Split(',');
                    string number = split[1];
                    number = number.Replace("\"", "");
                    string name = split[3];
                    name = name.Replace("\"", "");
                    name = name.Remove(name.Length - 3);

                    if (!Phone.Contact.NumberExists(number))
                        new Phone.Contact(name, number);
                }
            }
            catch
            {
                success = false;
            }

            if(port != null)
                port.Close();
            if(!success)
            {
                //Phone.Contacts.Clear();
                //ImportPhonebook();
                //return;
            }
            Phone.Contacts.Sort();
        }

        public static class MediaPlayer
        {
            public static double Position
            {
                get
                {
                    if (player == null)
                        return 0;

                    try
                    {
                    return (double)player.Position / 1000;
                    }
                    catch (Exception) { return 0; }
                }
            }

            public static PlayerState State
            {
                get
                {
                    if (player == null)
                        return PlayerState.STOPPED;

                    string state = "";
                    try { state = player.Status; } catch (Exception) { return PlayerState.STOPPED; }
                    switch(state)
                    {
                        case "paused":
                            return PlayerState.PAUSED;
                        case "playing":
                            return PlayerState.PLAYING;
                    }

                    return PlayerState.STOPPED;
                }
            }
                
            public static double TrackDuration
            {
                get
                {
                    if (player == null)
                        return 0;

                    IDictionary<string, object> track = null;
                    try { track = player.Track; } catch(Exception) { return 0; }
                    UInt32 duration = (UInt32)track["Duration"];
                    return (double)duration / 1000;
                }
            }

            public static string TrackArtist
            {
                get
                {
                    if (player == null)
                        return "";

                    IDictionary<string, object> track = null;
                    try { track = player.Track; } catch(Exception) { return ""; }
                    return (string)track["Artist"];
                }
            }

            public static string TrackTitle
            {
                get
                {
                    if (player == null)
                        return "";

                    IDictionary<string, object> track = null;
                    try { track = player.Track; } catch(Exception) { return ""; }
                    return (string)track["Title"];
                }
            }

            public static void Play()
            {
                if (player == null)
                    return;

                player.Play();
            }
            public static void Pause()
            {
                if (player == null)
                    return;

                player.Pause();
            }
            public static void Next()
            {
                if (player == null)
                    return;

                player.Next();
            }
            public static void Previous()
            {
                if (player == null)
                    return;

                player.Previous();
            }
        }
    }
}
