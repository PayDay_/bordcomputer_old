﻿using OpenTK;
using System;
using System.Collections.Generic;

namespace Bordcomputer
{
    public class Transform
    {
        //--------------------- STATIC ----------------------//
        public static List<Transform> Transforms = new List<Transform>();

        public static void UpdateTransforms()
        {
            foreach (Transform tf in Transforms)
            {
                UpdateTransform(tf);
            }
        }

        private static void UpdateTransform(Transform tf)
        {
            if (!tf.updateMatrix)
                return;

            tf.globalPosition = tf.Position;
            if (tf.Parent != null)
            {
                UpdateTransform(tf.Parent);
                tf.globalPosition += tf.Parent.GlobalPosition;
            }

            Vector2 calcPosition = tf.GlobalPosition;
            calcPosition.Y = (float)Program.SCREEN_HEIGHT - calcPosition.Y;
            calcPosition.Y = calcPosition.Y - tf.Size.Y;

            tf.Matrix = Matrix4.CreateScale(tf.Size.X, tf.Size.Y, 1);

            tf.Matrix *= Matrix4.CreateTranslation(new Vector3(-tf.Size.X / 2, -tf.Size.Y / 2, 0));
            tf.Matrix *= Matrix4.CreateRotationZ(tf.Rotation * (float)(Math.PI / 180));
            tf.Matrix *= Matrix4.CreateTranslation(new Vector3(tf.Size.X / 2, tf.Size.Y / 2, 0));

            tf.Matrix *= Matrix4.CreateTranslation(new Vector3(calcPosition));

            tf.updateMatrix = false;
        }

        //--------------------- INSTANCE ----------------------//
        public Matrix4 Matrix;

        private Vector2 position;
        public Vector2 StartPosition;
        public virtual Vector2 Position
        {
            set { position = value; Invalidate(); }
            get { return position; }
        }
        private float rotation;
        public virtual float Rotation
        {
            set { rotation = value; Invalidate(); }
            get { return rotation; }
        }
        private Vector2 size;
        public virtual Vector2 Size
        {
            set { size = value; Invalidate(); }
            get { return size; }
        }

        private Transform parent;
        public virtual Transform Parent
        {
            get { return parent; }
            set
            {
                if (parent != null)
                    parent.Children.Remove(this);

                parent = value;

                if (parent != null)
                    parent.Children.Add(this);

                Invalidate();
            }
        }
        public List<Transform> Children = new List<Transform>();

        private Vector2 globalPosition;
        public Vector2 GlobalPosition { get { return globalPosition; } }

		private bool visible = true;
		public virtual bool Visible
		{
			get
            {
                if (Parent != null)
                    return Parent.Visible && visible;
                else
                    return visible;
            } 
			set 
			{
                visible = value;
                VisibleChanged(value);
            }
		}

        private bool updateMatrix;

        public Transform(Vector2 position, Vector2 size, Transform parent = null)
        {
            Position = position;
            StartPosition = position;
            Size = size;
            Parent = parent;

            Transforms.Add(this);
        }

        private void Invalidate()
        {
            updateMatrix = true;
            foreach (Transform child in Children)
                child.Invalidate();
        }

        protected virtual void VisibleChanged(bool visible)
        {
            foreach (Transform child in Children)
                child.VisibleChanged(visible);
        }

        public void SetDescendantsVisible(bool visible)
        {
            Visible = visible;
            foreach (Transform child in Children)
                child.SetDescendantsVisible(visible);
        }
    }
}
