﻿using OpenTK;
using System.Collections.Generic;
using OpenTK.Input;

namespace Bordcomputer
{
    public abstract class Application
	{
		public static List<Application> Applications = new List<Application> ();
        public Component Root;

        public string Name;
        public string Icon;

		public bool Visible
		{
			get
			{
				return Root.Visible;
			}
			set
			{
				Root.Visible = value;
			}
		}

        //Sliding app away
		protected bool sliding;
        protected float slidingProgress;

        public Application()
        {
            Applications.Add(this);

            OnLoad();
        }

        public virtual void OnLoad ()
        {
            SlideRight.OnGesture += new SlideRight.GestureDelegate(OnSlideRight);
        }
		public virtual void OnUpdateFrame ()
        {
            UpdateSliding();
        }
		public abstract void OnOpen ();

        public abstract void OnKeyDown(KeyboardKeyEventArgs e);

        public virtual void OnDown(Vector2 position)
        {
        }
        public virtual void OnMove(Vector2 position)
        {
        }
        public virtual void OnUp(Vector2 position)
        {
        }

		protected virtual void OnSlideRight(object sender)
        {
            if (!Visible)
                return;
            if (sliding)
                return;
            if (SliderVertical.DraggedCount > 0 || SliderHorizontal.DraggedCount > 0)
                return;

            Mainframe.AppLauncher.Root.Position = new Vector2(-Mainframe.AppLauncher.Root.Size.X, Mainframe.AppLauncher.Root.Position.Y);
            sliding = true;
        }

        void UpdateSliding()
        {
            if (!sliding)
                return;

            if(!Mainframe.AppLauncher.Visible)
                Mainframe.AppLauncher.Visible = true;

            float posX = 0;

            posX = MathHelper.SmoothStep(0, Root.Size.X, slidingProgress);

            Root.Position = new Vector2(posX, Root.Position.Y);
            Mainframe.AppLauncher.Root.Position = new Vector2(posX - Mainframe.AppLauncher.Root.Size.X, Mainframe.AppLauncher.Root.Position.Y);

            slidingProgress += (float)Program.ElapsedSeconds * 2;
            if (slidingProgress >= 1)
            {
                this.Visible = false;

                Mainframe.AppLauncher.Root.Position = Vector2.Zero;
                Root.Position = Vector2.Zero;

                sliding = false;
                slidingProgress = 0;
            }
        }
    }
}

