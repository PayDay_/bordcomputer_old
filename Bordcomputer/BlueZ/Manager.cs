﻿using System;
using System.Collections.Generic;
using DBus;

namespace org.ofono
{
    // on /org/ofono/
    [Interface("org.ofono.Manager")]
    public interface Manager
    {
        IDictionary<ObjectPath,IDictionary<string,IDictionary<string,object>>> GetManagedObjects();
        ObjectPath[] GetModems();
    }
}