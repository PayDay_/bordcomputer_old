﻿using System;
using System.Collections.Generic;
using DBus;

namespace org.bluez
{
    // on /org/bluez/hciX/dev_XX_XX_XX_XX_XX_XX/playerX
    [Interface("org.bluez.MediaPlayer1")]
    public interface MediaPlayer1
    {
        void FastForward();
        void Next();
        void Pause();
        void Play();
        void Previous();
        void Rewind();
        void Stop();

        bool Browsable { get; }
        IDictionary<string, object> Track { get; }
        string Status { get; }
        UInt32 Position { get; }
    }
}
