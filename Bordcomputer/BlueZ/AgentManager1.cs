using System;
using System.Collections.Generic;
using NDesk.DBus;

namespace org.bluez
{
	// on /org/bluez
	[Interface("org.bluez.AgentManager1")]
	public interface AgentManager1
	{
		void RegisterAgent(ObjectPath agent,string capability);
		void RequestDefaultAgent(ObjectPath agent);
		void UnregisterAgent(ObjectPath agent);
	}
}
