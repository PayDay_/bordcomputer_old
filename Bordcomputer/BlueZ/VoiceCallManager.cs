﻿using System;
using System.Collections.Generic;
using DBus;

namespace org.ofono
{
    // on /org/ofono/bluez/hciX/dev_XX_XX_XX_XX_XX_XX
    [Interface("org.ofono.VoiceCallManager")]
    public interface VoiceCallManager
    {
        ObjectPath Dial(string number, string hide_callerid);
    }
}