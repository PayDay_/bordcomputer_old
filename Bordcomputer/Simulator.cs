﻿using System;
using System.Threading;

namespace Bordcomputer
{
	public static class Simulator
	{
		static float currentLerpTime;
		static float lerpTime;

		public static float Speed;
		public static float RPM;
		public static float Throttle;
		public static float Consumption;
		public static float Tank;

		public static void Init()
		{
			Console.WriteLine ("Initializing Simulator...");

			lerpTime = 1;
			Speed = 50;

			new Thread(new ThreadStart(Update)).Start();
		}

		static void Update()
		{
			while (true)
			{
				/*speedTimer += (float)Mainframe.deltaTime;

				if(speedTimer >= 1)
				{
					UpdateSpeed ();
					speedTimer = 0;
				}

				UpdateTank ();*/
				currentLerpTime += (float)Program.ElapsedSeconds;

				Tank = (float)System.Math.Sin(currentLerpTime * Math.PI * 0.5F) * 50;
				Tank += 50;

				Speed = Tank * 2;
				RPM = Tank * 80;
				Throttle = Tank;
				Consumption = Tank * 0.3f;

				Thread.Sleep (10);
				//Thread.Sleep (10);
			}
		}

		static void UpdateTank()
		{
			currentLerpTime += (float)Program.ElapsedSeconds / 6;

			float t = currentLerpTime / lerpTime;
			t = (float)System.Math.Sin (t * System.Math.PI * 0.5F);
			t += 1;
			t /= 2;

			Tank = t * 100;
		}

		static void UpdateSpeed()
		{
			Random random = new Random ();
			float diff = (float)random.NextDouble () * 10;
			diff -= 5;

			Speed += diff;
		}
	}
}

